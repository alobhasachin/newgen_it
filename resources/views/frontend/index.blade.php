@include('frontend/layout/header')
    <!-- start slider -->
    <div id="demo" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @php $count = 1; @endphp
            @if(!empty($slider_banners))
            @foreach($slider_banners as $banner)
            <div class="carousel-item {{($count == 1)?'active':''}}">
                <img src="{{url($banner->banner_image)}}" alt="education" class="img-fluid">
            </div>
            @php $count++; @endphp
            @endforeach
            @endif
        </div>
        <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
            <li data-target="#demo" data-slide-to="3"></li>
        </ul>
    </div>
    <!-- end slider -->
        <!-- start target -->
    <section class="service-box text-center">
        <div class="container">
            <div class="row">
                @if(!empty($services))
                @foreach($services as $service)
                <div class="col-md-4">
                    <div class="target-block text-white p-3 mb-lg-0 mb-xl-0 mb-md-0 mb-sm-5 mb-5">
                        @if(!empty($service->image))
                        <img class="target-icon" src="{{url($service->image)}}" style="width: 50px;">
                        @endif
                        <!-- <i class="fa fa-users fa-3x color-orange mb-3 target-icon"></i> -->
                        <h4 class="">{{$service->name??''}}</h4>
                        <p class="text-justify">{{$service->description??''}}</p>
                    </div>
                </div>
                @endforeach
                @endif

            </div>
        </div>
    </section>
    <!-- end target -->
    <!-- start counter -->
    <section class="counter-section mt-5">
        <div class="container">
            <div class="counter-box ">
                <h3 class="text-white">Doing The Right Thing, At The Right Time.</h3>
                <div class="row mt-5">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="text-white">
                            <h2 class="timer count-number" data-to="87" data-speed="1500"></h2>
                            <h5 class="count-text mt-2">Successful Cases</h5>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="text-white">
                            <h2 class="timer count-number" data-to="11" data-speed="1500"></h2>
                            <h5 class="count-text mt-2">Consultants</h5>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="text-white">
                            <h2 class="timer count-number" data-to="7" data-speed="1500"></h2>
                            <h5 class="count-text mt-2">Awards and Recognitions</h5>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="text-white">
                            <h2 class="timer count-number percent" data-to="100" data-speed="1500"></h2>
                            <h5 class="count-text mt-2">Customer Satisfaction</h5>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- end counter -->
    <!-- start about us -->
    <section class="about-us text-md-left">
        <div class="container">
             <div class="row">
                <div class="col-lg-7">
                    <div class="pt-md-5">
                     <div class="sec-title">
                        <h2 class="text-blue text-left">{{$about_us->title??''}}</h2>
                    </div>
                    <p class="text-justify">{!!$about_us->description??''!!}</p>
                    <a href="{{route('about-us')}}" class="btn theme-orange theme-btn my-2">Read More</a>  
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="about-img">
                        @if(!empty($about_us->image))
                        <div class="img_1 aos-init aos-animate d-block m-auto" data-aos="zoom-in">
                            <img src="{{url($about_us->image)}}" class="img-fluid" alt="About">
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end about us -->
    <!-- start counter -->
    <section class="counter-section">
        <div class="container">
            <div class="counter-box ">
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <div class="text-md-left">
                            <h2 class="text-white looking-heading">Looking For A Consultant?</h2>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="text-md-right">
                            <a href="contact.php" class="btn theme-white theme-btn my-2 font-weight-bold">Contact Us</a>  
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- end counter -->
    <!-- start about us -->
    <section class="about-us text-left">
        <div class="container">
             <div class="row">
                <div class="col-lg-12">
                    <div class="pt-md-5">
                     <div class="sec-title">
                        <h2 class="text-blue text-left">{{$Hr_solutions->main_title??''}}</h2>
                    </div>
                    <ul class="explore_list">
                        {!!$Hr_solutions->main_description??''!!}
                    </ul>
                    <h4 class="text-blue text-left hr-heading">{{$Hr_solutions->sub_title??''}}</h4>
                    <ul class="explore_list">
                       {!!$Hr_solutions->sub_description??''!!}
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end about us -->
    <!-- start counter -->
    <section class="counter-section">
        <div class="container">
            <div class="counter-box ">
                <h2 class="text-white text-left pl-2 cont-heading">Leave us a message and we’ll call you back!</h2>
                <h5 class="text-white text-left pl-2">I would like to discuss:</h5>
                <form class="">
                    <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Full Name" required onkeypress="return alphaonly(event);">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Organisation" required>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Phone Number" onkeypress="return numbersonly(event);" required maxlength="10" minlength="10">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <input type="email" name="" class="form-control" placeholder="Email" onkeyup="validatehome(this.value);" onkeypress="return AvoidSpace(event)" required>
                            <span id="check" style="font-size: 10px;color: white !important;position: absolute;bottom: 17px;margin-bottom: 5px;left: 35px;"></span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                            <select class="form-control" required>
                                <option>HR Related</option>
                                <option>IT Related</option>
                                <option>Business Related</option>
                                <option>Others</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="form-group p-2" style="background: #fff;border-radius: 7px;">
                            <div class="row">
                                <div class="col-6 text-left pl-4">
                                    <div style="top: 50%;position: absolute;transform: translatey(-50%);">
                                        <label style="font-size: 18px;"><input type="checkbox" name="" class="btn check-btn mr-3" required>I'm not a robot</label>
                                    </div>
                                    
                                </div>
                                <div class="col-6 text-right pr-4">
                                    <div class="text-center" style="display: inline-block;line-height: 1;">
                                        <img src="assets/images/recaptcha.png">
                                    <p class="m-0">reCAPTCHA</p>
                                    <a href="javascript:void(0);" style="font-size: 10px;margin: 0;">Privacy.Terms</a>
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="text-center">
                            <button style="width: 200px;" class="btn theme-white theme-btn my-2 font-weight-bold">Submit</button>  
                        </div>
                    </div>
                 </div>
                </form>
                
            </div>

        </div>
    </section>
    <!-- end counter -->
    <!-- start map section -->
    <section class="about-us pt-4 pb-4"  style="line-height: 0;">
        <div class="container-fluid">
             <div class="row">
               <div class="owl-carousel" id="partner-carousel">
                  <div>
                       <div class="partner-img p-2">
                          <img src="assets/images/1.png">
                      </div>
                  </div>
                  <div>
                       <div class="partner-img p-2">
                          <img src="assets/images/2.png">
                      </div>
                  </div>
                  <div>
                       <div class="partner-img p-2">
                          <img src="assets/images/3.png">
                      </div>
                  </div>
                  <div>
                       <div class="partner-img p-2">
                          <img src="assets/images/4.png">
                      </div>
                  </div>
                  <div>
                       <div class="partner-img p-2">
                          <img src="assets/images/5.png">
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end about us -->

<!-- start map -->
    <section class="counter-section p-0">
        <div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3506.559309968242!2d77.14304761503311!3d28.492815482473073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1f8ab86c668d%3A0xcdf3cb7434fc7ca0!2sNewGen%20IT%20solutions%20and%20Services!5e0!3m2!1sen!2sin!4v1591014741767!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        <div class="container-fluid">
           <div class="map">
            
               <div class="owl-carousel" id="map-carousel">
                @php
                if(!empty($contact_details->contact_info)){
                    $addresses = json_decode($contact_details->contact_info);
                }else{
                    $addresses = [];
                }
                @endphp
                @if(!empty($addresses))
                @foreach($addresses as $addres)
                    <div>
                        <div class="p-3 text-white">
                                <h3>{{$addres->country??''}}</h3>
                                <p>{{$addres->address}}</p>
                                <a href="mailto:sales@newgenit.com">{{$addres->email}}</a>
                            </div>
                        </div>
                        @endforeach
                        @endif
                  </div>
           </div>
        </div>
    </section>
    <!-- end map -->

@include('frontend/layout/footer')




















































