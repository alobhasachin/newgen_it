@include('frontend/layout/header')
<style type="text/css">
    .career-banner{
     background: url({{$banners[0]->career_banner}});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
}
</style>
    <!-- start inner-banner -->
    <section class="inner-banner career-banner">
        <h1 class="font-weight-bold text-center">Career</h1>
    </section>
    <!-- end inner-banner -->

    <!-- start about us -->
    <section class="pb-0 pt-5">
        <div class="container">
            <div class="sec-title">
                        <h2 class="text-blue text-center career-heading">{{$career_details->title1??''}}</h2>
                    </div>
            <div class="row">
                <div class="col-md-12">
                  <p class="text-justify">{!!$career_details->title1_desc??''!!}</p>
                </div>
            </div>
        </div>
    </section>
    <!-- end about us -->

    <!-- start about us -->
    <section class="pt-4 pb-0">
        <div class="container">
            <div class="sec-title">
                        <h2 class="text-blue text-center career-heading">{{$career_details->title2??''}}</h2>
                    </div>
            <div class="row">
                <div class="col-md-12">
                    {!!$career_details->title2_desc??''!!}
                </div>
            </div>
        </div>
    </section>
    <!-- end about us -->

    <section class="aboutus">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="sec-title">
                        <h3 class="text-blue text-center">We Are Hiring</h3>
                    </div>
                    <div id="accordion" class="accordion">
        <div class="card mb-0">
            @if(!empty($jobs))
            @foreach($jobs as $job)
            <div class="card-header collapsed" data-toggle="collapse" href="#collapse{{$job->id}}">
                <a class="card-title">
                   {{$job->job_title??''}}
                </a>
            </div>
            <div id="collapse{{$job->id}}" class="card-body collapse" data-parent="#accordion" >
                <h5>Job Description</h5>
                <ul>
                    {!!$job->description??''!!}
                </ul>
            </div>
            @endforeach
           @endif
            
        </div>
    </div>
                </div>
                <div class="col-lg-6">
                    <div class="sec-title">
                        <h3 class="text-blue text-center">Apply Here</h3>
                    </div>
                    <div class="">
                    <form class="contact_form">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Your Name" required onkeypress="return alphaonly(event);">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="email" name="" class="form-control" placeholder="Your Email Address" onkeyup="validate(this.value);" onkeypress="return AvoidSpace(event)" required>
                            <span id="check" style="font-size: 10px;color: red;position: absolute;bottom: 0;margin-bottom: 5px;"></span>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Your Mobile Number" onkeypress="return numbersonly(event);" required maxlength="10" minlength="10">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Your Designation" required>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" placeholder="Leave Message" required></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="file" class="form-control" name="" accept=".pdf, .doc" required>
                            <span style="position: absolute;top: 50px;right: 15px;">File Type(pdf,doc)</span>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="text-center">
                            <button style="width: 200px; border-radius:0 !important;" class="btn theme-orange theme-btn my-2 font-weight-bold">Submit</button>  
                        </div>
                    </div>
                 </div>
                </form>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <!-- start about us -->
    <section class="aboutus pt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>By taking the time to understand your goals, interests, and skills, NewGen It Solutions will match you with the most relevant and desirable IT jobs for you.</p>
                    <p class="mb-0">Contact our in Country Recruitment Team for job opening OR Submit Your Resume at</p>
                    <p class="mt-0"><a class="text-blue" href="javascript:void(0);">jobs@newgenit.com</a> .Out Talent Acquisition team will contact you within seven (07) working days.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- end about us -->
    <!-- start counter -->
    <section class="counter-section">
        <div class="container">
            <div class="counter-box ">
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <div class="text-md-left">
                            <h2 class="text-white looking-heading">Looking For A Consultant?</h2>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="text-md-right">
                            <a href="contact.php" class="btn theme-white theme-btn my-2 font-weight-bold">Contact Us</a>  
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- end counter -->


@include('frontend/layout/footer')








































