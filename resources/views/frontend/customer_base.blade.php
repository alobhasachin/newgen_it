@include('frontend/layout/header')
<style type="text/css">
    .customer-banner{
     background: url({{$banners[0]->customer_banner}});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
}
</style>

    <!-- start inner banner -->
    <section class="inner-banner customer-banner">
        <h1 class="font-weight-bold text-center">Customer Base</h1>
    </section>
    <!-- end inner banner -->


    <!-- start counter -->
    <section class="customer-section mt-3 mb-3 pt-3 pb-3">
        <div class="container">
            <div class="row justify-content-center">
        @php $count = 1; @endphp
        @if(!empty($customers))
        @foreach($customers as $image)  
                <div class="col-lg col-md-3 col-6 mb-3 mb-lg-0">
                <img src="{{url($image->customer_image)}}" class="img-fluid rounded-corner">
                </div>
        @if($count%5 == 0)
            </div>
        </div>
        @if(count($customers) > $count)
        <div class="container-fluid p-2 mt-3 mb-3" style="background: #fff;"></div>
        <div class="container">
            <div class="row justify-content-center">
        @endif
        @endif
        @php $count++; @endphp
        @endforeach
        @endif
    </section>
    <!-- end counter -->

 
@include('frontend/layout/footer')


























































