    <!-- start footer -->
    <footer class="theme-blue">
        <div class="container-fluid">
            <div class="footer-top border-bottom pt-5">
                <div class="row">
                    <div class="col-lg-3 col-md-12">
                        <a href="javascript:void(0);">
                            <h1 class="text-uppercase mb-4">newgenit</h1>
                            <!-- <img src="assets/images/logo.png" class="img-fluid mb-3 footer_logo" alt="Edusquad"> --></a>
                        <p>NewGen IT is a one STOP IT services provider. Our clients include food industry, IT giants, technology giants among other companies in the Energy, IT, Telecommunications, Education, Healthcare and Consumer sector.</p>
                        
                    </div>
                    <div class="col-lg-9 col-md-12">
                        <div class="row">
                            <div class="col-lg-1 d-lg-block d-md-none"></div>
                    <div class="col-lg-5 col-md-8">
                        <h1 class="footer-heading mb-4">Our Offerings</h1>
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                        <ul class="address-icon">
                            <li class="mb-2"><a href="javascript:void(0);">IT Managed Services</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">Service Delivery</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">Hardware & Software Procurement</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">Human Resource Recruitment Services</a></li>
                        </ul>
                    </div>

                    
                    <div class="col-lg-6 col-md-6 mb-md-5 mb-4">
                        <ul class="address-icon">
                            <li class="mb-2"><a href="javascript:void(0);">Technology Management</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">Cyber Security</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">Asset Management</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">HR Advisory Services</a></li>
                        </ul>
                    </div>
                        </div>
                    </div>
                    <div class="col-lg-1 d-lg-block d-md-none"></div>
                    <div class="col-lg-5 col-md-4 pl-lg-5  mb-md-5 pr-lg-5">
                        <h1 class="footer-heading mb-4">About Us</h1>
                        <ul class="address-icon">
                            <li class="mb-2"><a href="javascript:void(0);">Who We Are</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">Core Values</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">Mission/Vision</a></li>
                            <li class="mb-2"><a href="javascript:void(0);">Leadership</a></li>
                        </ul>
                        <ul class="social-icon">
                            <li><a href="javascript:"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="javascript:"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="javascript:"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="javascript:"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="bottom-footer py-3 text-center">
                <p class="mb-0 text-center">Copyright © 2018 NEWGEN IT</p>
                <!-- <ul class="list-inline mb-0">
                    <li class="list-inline-item"><a href="javascript:">Terms of use</a></li>
                    <li class="list-inline-item"><a href="javascript:">Privacy policy</a></li>
                </ul> -->
            </div>
        </div>
    </footer>
    <!-- end footer -->
    <a href="#" id="scroll"><span></span></a>
</div>
<!-- ===============jQuery Frameworks============= -->
<script src="{{url('assets/js/jquery.min.js')}}"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/slick.min.js')}}"></script>
<script src="{{url('assets/js/jquery.fancybox.min.js')}}"></script>
<script src="{{url('assets/js/aos.js')}}"></script>
<script src="{{url('assets/js/script.js')}}"></script>
<script src="{{url('assets/js/page/home/home.js')}}"></script>
<script src="{{url('assets/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
    $('#partner-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    autoplay:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:false
        },
        600:{
            items:3,
            loop:true,
            nav:false
        },
        1000:{
            items:5,
            loop:true,
            nav:false
        }
    }
})
</script>
<script type="text/javascript">
    $('#map-carousel').owlCarousel({
        loop:true,
    margin:0,
    nav:true,
    autoplay:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})
</script>

<script type="text/javascript">
 function myFunction($a) {
        var html1=$a;
        jQuery("#InputSubject").val(html1);
    } 
            function isNumberKey(evt)
              {
//            var charCode = (evt.which) ? evt.which : event.keyCode
//            if (charCode > 31 && (charCode < 48 || charCode > 57))
//            return false;
//            return true;

 var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
  }
              


          function alphaonly(evt) {
          var keyCode = (evt.which) ? evt.which : evt.keyCode
          if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
          return false;  
        }    


        function alphanumeric(e) {               
            var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            specialKeys.push(9); //Tab
            specialKeys.push(46); //Delete
            specialKeys.push(36); //Home
            specialKeys.push(35); //End
            specialKeys.push(37); //Left
            specialKeys.push(39); //Right

            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            // document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        }   
</script>

<script type="text/javascript">


/** add active class and stay opened when selected */
var url = window.location;

// for sidebar menu entirely but not cover treeview
$('.sidenav-links a').filter(function() {
   return this.href == url;
}).closest('.sidenav-links a').addClass('active');

</script>
<script type="text/javascript">


/** add active class and stay opened when selected */
var url = window.location;

// for sidebar menu entirely but not cover treeview
$('ul.navbar-nav a').filter(function() {
   return this.href == url;
}).parent().addClass('active');

</script>
<script>
/************Number and dash allowed************/
function dashnumbersonly(e) {
        var unicode = e.charCode? e.charCode : e.keyCode
        if (unicode!=8 && unicode!=45) { //if the key isn't the backspace key (which we should allow)
            if (unicode<48||unicode>57) //if not a number
            return false //disable key press
        }
    }
/************Numbers only *************/
        function numbersonly(e) {
 var k = event ? event.which : window.event.keyCode;
            if (k == 32) return false;
                var unicode=e.charCode? e.charCode : e.keyCode;

                if (unicode!=8) { //if the key isn't the backspace key (which we should allow)
                    if (unicode<48||unicode>57) //if not a number
                    return false //disable key press
                }
            }
      
/********************Number with decimal***********/

$('.allownumer_decimal').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
    /************Alphabet only *************/
        function alphaonly(evt) {
          var keyCode = (evt.which) ? evt.which : evt.keyCode
          if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
          return false;  
        }  

    /************Alpha numeric only *************/   
           function alphanumnotspace(e) {               
            var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            specialKeys.push(9); //Tab
            specialKeys.push(46); //Delete
            specialKeys.push(36); //Home
            specialKeys.push(35); //End
            specialKeys.push(37); //Left
            specialKeys.push(39); //Right
             var k = event ? event.which : window.event.keyCode;
            if (k == 32) return false;
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            // document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        } 

        function alphanumeric(e) {               
            var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            specialKeys.push(9); //Tab
            specialKeys.push(46); //Delete
            specialKeys.push(36); //Home
            specialKeys.push(35); //End
            specialKeys.push(37); //Left
            specialKeys.push(39); //Right

            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            // document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        } 
function ValidateEmail(inputText)
{
var mailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
if(inputText.value.match(mailformat))
{
alert("You have entered a valid email address!");    //The pop up alert for a valid email address
document.form1.text1.focus();
return true;
}
else
{
alert("You have entered an invalid email address!");    //The pop up alert for an invalid email address
document.form1.text1.focus();
return false;
}
}

</script>
<script language="JavaScript">
            var ev = /^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(gmail|yahoo)\.com$/;
            var x = document.getElementById("check");
            function validate(email){
            if(!ev.test(email))
                {
                    x.innerHTML = "Not a valid email";
                    x.style.color = "red"
                }
            else
                {
                    x.innerHTML = "Looks good!";
                    x.style.color = "green"
                }
            }
        </script>
        <script language="JavaScript">
            var ev = /^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\.)?[a-zA-Z]+\.)?(gmail|yahoo)\.com$/;
            var x = document.getElementById("check");
            function validatehome(email){
            if(!ev.test(email))
                {
                    x.innerHTML = "Not a valid email";
                    x.style.color = "white"
                }
            else
                {
                    x.innerHTML = "Looks good!";
                    x.style.color = "white"
                }
            }
        </script>
        <script type="text/javascript">
           function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32) return false;
}


        </script>
</body>

<!-- Mirrored from radixtouch.in/flexweb_template/edusquad-preview/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Mar 2020 10:12:06 GMT -->
</html>
