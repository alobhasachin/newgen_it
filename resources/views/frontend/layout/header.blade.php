<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from radixtouch.in/flexweb_template/edusquad-preview/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Mar 2020 10:09:02 GMT -->
<head>
    <!-- ========== Meta Tags ========== -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="NewGen IT">  
    <!-- ========== Page Title ========== -->
    <title>NEWGENIT</title>
    <!-- ========== Favicon Icon ========== -->
    <link rel="shortcut icon" href="{{url('assets/images/favicon.png')}}" type="image/x-icon">
    <!-- ========== Google Fonts ========== -->
    <link href="{{url('assets/css/font.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;500;600;700&display=swap" rel="stylesheet">
    <!-- ========== Start Stylesheet ========== -->
    <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/slick.css')}}"/>
    <link rel="stylesheet" href="{{url('assets/css/slick-theme.css')}}"/>
    <link rel="stylesheet" href="{{url('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/aos.css')}}"/>
    <link rel="stylesheet" href="{{url('assets/css/jquery.fancybox.min.css')}}"/>
    <link rel="stylesheet" href="{{url('assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/owl.carousel.min.css')}}">
</head>
<!--start preloader-->
<!-- <div class="preloader">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div> -->
<!--end preloader-->
<div class="page-wrapper" id="page-wrapper">
    <!-- start top header -->
    <header class="top-header">
        <div class="container-fluid">
            <div class="row align-content-center">
                <div class="col-xl-7 col-lg-5 col-md-6">
                    <p class="marquee"><span>
                        Our Presence in Singapore, Australia, Hong Kong, UAE, India. NewGen IT recently hosted First Customer Success Event on 17th January 2020</span>
                    </p>
                </div>
                <div class="col-xl-3 col-lg-5 col-md-6 py-2 f-15 announcement">
                    <span>sales@newgenit.com, hr@newgenit.com</span>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-12 text-left top-btn">
                    <ul class="social-icon mb-0" style="line-height: 3;">
                            <li><a href="javascript:"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="javascript:"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="javascript:"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="javascript:"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- end top header -->
    <!-- start main header -->
    <header class="main-header" id="header">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <a class="navbar-brand" href="index.php">
                    <img src="{{url('assets/images/logo.png')}}" class="img-fluid logo_img" alt="">
                </a>
                <span class="navbar-toggler">
                    <i class="ti-align-left" onclick="openNav()"></i>
                </span>
                <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('/')}}">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="{{route('about-us')}}">About Us</a>
                        </li>
                       <!--  <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                Student Corner
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="result.php">Result</a>
                                <a class="dropdown-item" href="student.php">Student</a>
                                <a class="dropdown-item" href="course.php">Course</a>
                            </div>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('/')}}">Our Offerings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('our-partners')}}">Our Partners</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('customer-base')}}">Customer Base</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('careers')}}">Career</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('contact-us')}}">Contact Us</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- end main header -->

        <!-- start side menu -->
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="sidenav-links pt-4">
           <a class="nav-link" href="{{route('/')}}">Home</a>
            <a class="nav-link" href="{{route('about-us')}}">About Us</a>
            <a class="nav-link" href="{{route('/')}}">Our Offerings</a>
            <a class="nav-link" href="{{route('our-partners')}}">Our Partners</a>
            <a class="nav-link" href="{{route('customer-base')}}">Customer Base</a>
             <a class="nav-link" href="{{route('careers')}}">Career</a>
             <a class="nav-link" href="{{route('contact-us')}}">Contact Us</a>
            <!-- <button class="dropdown-btn">About Us
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-container">
                <a href="about.php"> <i class="fa fa-caret-right mr-3"></i>Who We Are</a>
                <a href="achievement.php"><i class="fa fa-caret-right mr-3"></i>Achievement</a>
            </div> -->
            
        </div>
    </div>
    <!-- end side menu -->

