@include('frontend/layout/header')
<style type="text/css">
    .contact-banner{
     background: url({{$banners[0]->contact_banner}});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
}
</style>
    <!-- start inner-banner -->
    <section class="inner-banner contact-banner">
        <h1 class="font-weight-bold text-center">Contact Us</h1>
    </section>
    <!-- end inner-banner -->
    <section class="text-center mb-4 mt-4">
        <div class="container">
            <div class="row justify-content-center">
                @php
                if(!empty($contact_details->contact_info)){
                    $addresses = json_decode($contact_details->contact_info);
                }else{
                    $addresses = [];
                }
                @endphp

                @if(!empty($addresses))
                @foreach($addresses as $addres)
                <div class="col-md-4 mb-3 mt-3">
                    <div class="contact-block p-3">
                        <h4>{{$addres->country}}</h4>
                        <i class="fa fa-map-marker fa-2x"></i>
                        <p>{{$addres->address}}</p>
                        <i class="fa fa-envelope fa-2x"></i>
                        <p>{{$addres->email}}</p>
                    </div>
                </div>
                @endforeach
                @endif

            </div>
        </div>
    </section>
    
    <section class="aboutus pt-0">
        <div class="container">
             <div class="sec-title">
                        <h2 class="text-blue text-center mb-5">Request A Query</h2>
                    </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-img">
                        @if(!empty($contact_details->contact_image))
                        <div class="aos-init aos-animate d-block m-auto" data-aos="zoom-in">
                            <img src="{{url($contact_details->contact_image)}}" class="img-fluid" alt="contact">
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="">
                    <form class="contact_form">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Full Name" required onkeypress="return alphaonly(event);">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Organisation" required>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" name="" class="form-control" placeholder="Phone Number" onkeypress="return numbersonly(event);" required maxlength="10" minlength="10">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="email" name="" class="form-control" placeholder="Email" onkeyup="validate(this.value);" onkeypress="return AvoidSpace(event)" required>
                            <span id="check" style="font-size: 10px;color: red;position: absolute;bottom: 0;margin-bottom: 5px;"></span>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <select class="form-control" required>
                                <option>HR Related</option>
                                <option>IT Related</option>
                                <option>Business Related</option>
                                <option>Others</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <div class="form-group p-2" style="background: #f6f6f6;border-radius: 7px;border: 1px solid #ced4da;">
                            <div class="row">
                                <div class="col-6 text-left pl-lg-4">
                                    <div style="top: 50%;position: absolute;transform: translatey(-50%);">
                                        <label style="font-size: 16px;"><input type="checkbox" required name="" class="btn check-btn mr-3">I'm not a robot</label>
                                    </div>
                                    
                                </div>
                                <div class="col-6 text-right pr-lg-4">
                                    <div class="text-center" style="display: inline-block;line-height: 1;">
                                        <img src="assets/images/recaptcha.png">
                                    <p class="m-0">reCAPTCHA</p>
                                    <a href="javascript:void(0);" style="font-size: 10px;margin: 0;">Privacy.Terms</a>
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="text-center">
                            <button style="width: 200px; border-radius:0 !important;" class="btn theme-orange theme-btn my-2 font-weight-bold">Submit</button>  
                        </div>
                    </div>
                 </div>
                </form>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@include('frontend/layout/footer')



























































