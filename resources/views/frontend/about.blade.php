@include('frontend/layout/header')
<style type="text/css">
    .about-banner{
     background: url({{$banners[0]->about_banner}});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
}
</style>
    <!-- start inner banner -->
    <section class="inner-banner about-banner">
        <h1 class="font-weight-bold text-center">About Us</h1>
    </section>
    <!-- end inner banner -->
    <!-- start about us -->
    <section class="aboutus">
        <div class="container">
            <div class="sec-title">
                        <h2 class="text-blue text-left">Who We Are</h2>
                    </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="">
                    <p class="text-justify">{{$details->title??''}}</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-img">
                        @if(!empty($details->main_image))
                        <div class="aos-init aos-animate d-block m-auto" data-aos="zoom-in">
                            <img src="{{url($details->main_image)}}" class="img-fluid" alt="About">
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-12">
                    <p class="text-justify">{!!$details->main_description!!}</p>
                </div>
            </div>
        </div>
    </section>
    <!-- end about us -->
    <section class="aboutus pt-0 corevalue">
        <div class="container">
            <div class="sec-title">
                        <h2 class="text-blue text-left">Core Values</h2>
                    </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="text-justify">{{$details->core_value??''}}</p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="core_tabs">
                        <ul class="row nav nav-tabs" style="position: relative;">
                            <li class="col-6 nav-item p-0"><a class="nav-link active" data-toggle="tab" href="#home">
                            <h6 style="margin-bottom:32px;">Teamwork</h6>
                            <div class="mt-5 pull-left">
                                <img style="width: 80px;height: 80px;" src="assets/images/teamwork-white.png" class="white-img">
                            <img style="width: 80px;height: 80px;" src="assets/images/teamwork-blue.png" class="blue-img">
                            </div>
                            <div class="clearfix"></div>
                            </a></li>
                            <li class="col-6 nav-item p-0 text-right"><a class="nav-link" data-toggle="tab" href="#menu1">
                            <h6>Entrepreneurial Spirit</h6>
                            <div class="mt-5 pull-right">
                                <img style="width: 80px;height: 80px;" src="assets/images/entrepreneurial-white.png" class="white-img">
                            <img style="width: 80px;height: 80px;" src="assets/images/entrepreneurial-blue.png" class="blue-img">
                            </div>
                            <div class="clearfix"></div>
                            
                            </a></li>
                            <li class="col-6 nav-item p-0"><a class="nav-link" data-toggle="tab" href="#menu2">
                            <h6>Social Responsibility</h6>
                            <div class="mt-5 pull-left">
                                <img style="width: 80px;height: 80px;" src="assets/images/social-white.png" class="white-img">
                            <img style="width: 80px;height: 80px;" src="assets/images/social-blue.png" class="blue-img">
                            </div>
                            <div class="clearfix"></div>
                            </a></li>
                            <li class="col-6 nav-item p-0 text-right"><a class="nav-link" data-toggle="tab" href="#menu3">
                            <h6 style="margin-bottom:32px;">Focus</h6>
                            <div class="mt-5 pull-right">
                                <img style="width: 80px;height: 80px;" src="assets/images/focus-white.png" class="white-img">
                            <img style="width: 80px;height: 80px;" src="assets/images/focus-blue.png" class="blue-img">
                            </div>
                            <div class="clearfix"></div>
                            </a></li>
                        </ul>
                        <div class="tabmid-icon">
                            <img src="assets/images/favicon.png">
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 col-sm-12">
                    <div class="tab-content">
    <div id="home" class="tab-pane active"><br>
      <h4 class="color-blue">Teamwork</h4>
      <p>{{$details->teamwork??''}}</p>
    </div>
    <div id="menu1" class="tab-pane fade"><br>
      <h4 class="color-blue">Entrepreneurial Spirit</h4>
      <p>{{$details->spirit??''}}</p>
    </div>
    <div id="menu2" class="tab-pane fade"><br>
      <h4 class="color-blue">Social Responsibility</h4>
      <p>{{$details->responsibility??''}}</p>
    </div>
    <div id="menu3" class="tab-pane fade"><br>
      <h4 class="color-blue">Focus</h4>
      <p>{{$details->focus??''}}</p>
    </div>
  </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end about us -->
    <!-- start counter -->
    <section class="counter-section abt-counter">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                   <div class="text-md-left text-white">
                       <h4 class="font-weight-bold">Our Mission</h4>
                       <p>{{$details->mission??''}}</p>
                   </div> 
                </div>
                <div class="col-md-6">
                   <div class="text-md-right text-white">
                       <h4 class="font-weight-bold">Our Vision</h4>
                       <p>{{$details->vision??''}}</p>
                   </div> 
                </div>
            </div>
            <div class="slintline"></div>
        </div>
    </section>
    <!-- end counter -->
    <!-- start leadership -->
    <section class="aboutus leadership">
        <div class="container">
            <div class="sec-title">
                        <h2 class="text-blue text-left">Leadership</h2>
                    </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="">
                    <p class="text-justify">{!!$details->description2??''!!}</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="about-img">
                        @if(!empty($details->image))
                        <div class="aos-init aos-animate d-block m-auto" data-aos="zoom-in">
                            <img src="{{url($details->image)}}" class="img-fluid" alt="leader" style="border:3px solid #196fa8;">
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end leadership -->
 
@include('frontend/layout/footer')


























































