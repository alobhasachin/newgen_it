@include('admin/layout/header')
@include('admin/layout/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Services
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Services</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Add Services</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('add-service')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Services Name<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control"  placeholder="Services Name" name="name"  required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea class="form-control" placeholder="Description" placeholder="Description" name="description"  required></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Icon Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" id="file" class="form-control" accept="image/jpg, image/jpeg, image/png" required>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Services List</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
              <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Service Name</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($services))
                @foreach($services as $service)
                <tr>
                  <td>{{$service->id}}</td>
                  <td>{{$service->name}}</td>
                  <td>
                  @if(!empty($service->image))  
                    <img src="{{url($service->image)}}" style="width: 60px; height: 40px; background: #8a8a8a;">
                  @endif
                  </td>
                  <td>
                    @if($service->status == 1)
                    <button class="btn btn-success btn-xs">Active</button>
                    @else
                    <button class="btn btn-danger btn-xs">Deactive</button>
                    @endif
                  

                  </td>
                  <td>
                    <div style="width: 140px;">
                    <button type="button" onclick="editService({{$service->id}})" class="btn btn-primary btn-xs edit_model" data-toggle="modal" data-target="#edit_service"><i class="fa fa-edit"></i></button>

                    <button type="button" onclick="deleteService({{$service->id}})" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    @if($service->status == 1)
                    <button class="btn btn-danger btn-xs" onclick="chnageStatus({{$service->id}})">Deactive</button>
                    @else
                    <button class="btn btn-success btn-xs" onclick="chnageStatus({{$service->id}})">Active</button>
                    @endif
                    
                  </a>
                </div>
                  </td>
                </tr> 
                @endforeach
                @endif             
              </tbody>
              </table>
            </div>
            </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <!-- edit modal -->
<div class="modal fade" id="edit_service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Service</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" action="{{route('update-service')}}" method="post" id="edit_model" enctype="multipart/form-data">
          @csrf
             <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Service Name<span style="color: red;">*</span></label>

                  <div class="col-sm-9">
                    <input type="hidden" name="service_id" id="service_id">
                    <input type="text" class="form-control" id="name_service" placeholder="Categories Name" name="name" value="IT Infrastructure Managed Services" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-9">
                     <textarea class="form-control" placeholder="Description" placeholder="Description" name="description"  id="desc_service" required>IT Infrastructure Managed Services</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Icon Image<span style="color: red;">*</span></label>

                  <div class="col-sm-9">
                    <input type="file" name="image" class="form-control" id="file" accept="image/jpg, image/jpeg, image/png">
                    <!-- <div onchange="fileValidation(this)"></div> -->
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label"></label>

                  <div class="col-sm-9" id="service_img">
                    <img src="dist/img/icon-1.png"  style="width: 100px;height: 100px; background: #8a8a8a;">
                  </div>
                </div>
              </div>
              <!-- /.
              /.box-body -->
              <div class="box-footer text-right">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>
</div>
 
@include('admin/layout/footer')
<script type="text/javascript">
    function chnageStatus(id){
      var token='{{csrf_token()}}';
      var service_id = id;
      $.ajax({
        url: "{{action('Admin\AdminController@changeServiceStatus')}}",
        method: "POST",
        dataType: "json",
        data:{'id':service_id, '_token':token},
        success: function(response) {
          if(response.status == 200){
            alert('Status updated successfully !');
            location.reload(true);
          }
        }
      }); 
    }

    function editService(id){
      var token='{{csrf_token()}}';
      var service_id = id;
      $.ajax({
        url: "{{action('Admin\AdminController@editService')}}",
        method: "POST",
        dataType: "json",
        data:{'id':service_id, '_token':token},
        success: function(response) {
          if(response.status == 200){
            $('#service_id').val(service_id);
            $('#name_service').val(response.name);
            $('#desc_service').val(response.desc);
            var image_url = '{{url('/')}}/'+response.img;
            $('#service_img').html('<img src="'+image_url+'"  style="width: 100px;height: 100px; background: #8a8a8a;">');
          }
        }
      }); 
    }

    function deleteService(id){
      var status = confirm('Are you sure..?');
      if(status == true){
        var token='{{csrf_token()}}';
        var service_id = id;
        $.ajax({
          url: "{{action('Admin\AdminController@deleteService')}}",
          method: "POST",
          dataType: "json",
          data:{'id':service_id, '_token':token},
          success: function(response) {
            if(response.status == 200){
              alert('Service deleted successfully !');
              location.reload(true);
            }
          }
        }) 
      }else{
        return false
      }
    }

  
</script>