 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!-- <b>Version</b> 2.4.0 -->
    </div>
    <strong>Copyright &copy; 2020 <a href="javascript:void(0);">NEWGENIT</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->

      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{url('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{url('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- datatables -->
<script src="{{url('bower_components/datatables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('bower_components/datatables-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('dist/js/adminlte.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{url('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap  -->
<script src="{{url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll -->
<script src="{{url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{url('bower_components/chart.js/Chart.js')}}"></script>

<script src="{{url('bower_components/ckeditor/ckeditor.js')}}"></script>
<!-- bootstrap datepicker -->

<script src="{{url('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script type="text/javascript" src="{{url('bower_components/datatables-bs/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{url('bower_components/datatables-bs/js/buttons.flash.min.js')}}"></script>
<script type="text/javascript" src="{{url('bower_components/datatables-bs/js/jszip.min.js')}}"></script>
<script type="text/javascript" src="{{url('bower_components/datatables-bs/js/pdfmake.min.js')}}"></script>
<script type="text/javascript" src="{{url('bower_components/datatables-bs/js/vfs_fonts.js')}}"></script>
<script type="text/javascript" src="{{url('bower_components/datatables-bs/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{url('bower_components/datatables-bs/js/buttons.print.min.js')}}"></script>

<script>
  $(function () {
    CKEDITOR.replace('editor1')
  })
   $(function () {
    CKEDITOR.replace('editor2')
  })
    $(function () {
    CKEDITOR.replace('editor3')
  })
</script>
<script>
  $(function (){
    $('#example1,#example2,#example3').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  })
</script>
<!-- <script type="text/javascript">
  $(document).ready(function() {
    $('#example1').DataTable( {
        dom: 'Bfrtip',
        buttons: [
             'csv', 'excel'
        ]
    } );
} );
</script> -->
<script type="text/javascript">
   //Date picker
    $(function () {
    $('#datepicker1').datepicker({
      autoclose: true
    })
  });

   //Date picker
    $(function () {
    $('#datepicker2').datepicker({
      autoclose: true
    })
  });
</script>

 <script type="text/javascript">
//     $(function(){
//     var current = location.pathname;
//     $('.treeview a').each(function(){
//         var $this = $(this);
        
//         if($this.attr('href').indexOf(current) !== -1){
//             $this.addClass('active');
//         }
//     })
// })
//     $(function() {
//      var url = window.location;
//     const allLinks = document.querySelectorAll('.treeview a');
//     const currentLink = [...allLinks].filter(e => {
//       return e.href == url;
//     });
    
//     currentLink[0].classList.add("active")
//     currentLink[0].closest(".treeview").style.display="block";
//     currentLink[0].closest(".treeview").classList.add("active");
// });


/** add active class and stay opened when selected */
var url = window.location;

// for sidebar menu entirely but not cover treeview
$('ul.sidebar-menu a').filter(function() {
   return this.href == url;
}).parent().addClass('active');

// for treeview
$('ul.treeview-menu a').filter(function() {
   return this.href == url;
}).closest('.treeview').addClass('active');
</script>
<script>
/************Number and dash allowed************/
function dashnumbersonly(e) {
        var unicode = e.charCode? e.charCode : e.keyCode
        if (unicode!=8 && unicode!=45) { //if the key isn't the backspace key (which we should allow)
            if (unicode<48||unicode>57) //if not a number
            return false //disable key press
        }
    }
/************Numbers only *************/
        function numbersonly(e) {
 var k = event ? event.which : window.event.keyCode;
            if (k == 32) return false;
                var unicode=e.charCode? e.charCode : e.keyCode;

                if (unicode!=8) { //if the key isn't the backspace key (which we should allow)
                    if (unicode<48||unicode>57) //if not a number
                    return false //disable key press
                }
            }
      
/********************Number with decimal***********/

$('.allownumer_decimal').keypress(function(event) {
    if (event.which != 46 && (event.which < 47 || event.which > 59))
    {
        event.preventDefault();
        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
            event.preventDefault();
        }
    }
});
    /************Alphabet only *************/
        function alphaonly(evt) {
          var keyCode = (evt.which) ? evt.which : evt.keyCode
          if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
          return false;  
        }  

    /************Alpha numeric only *************/   
           function alphanumnotspace(e) {               
            var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            specialKeys.push(9); //Tab
            specialKeys.push(46); //Delete
            specialKeys.push(36); //Home
            specialKeys.push(35); //End
            specialKeys.push(37); //Left
            specialKeys.push(39); //Right
             var k = event ? event.which : window.event.keyCode;
            if (k == 32) return false;
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            // document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        } 

        function alphanumeric(e) {               
            var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            specialKeys.push(9); //Tab
            specialKeys.push(46); //Delete
            specialKeys.push(36); //Home
            specialKeys.push(35); //End
            specialKeys.push(37); //Left
            specialKeys.push(39); //Right

            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
            // document.getElementById("error").style.display = ret ? "none" : "inline";
            return ret;
        } 
function ValidateEmail(inputText)
{
var mailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
if(inputText.value.match(mailformat))
{
alert("You have entered a valid email address!");    //The pop up alert for a valid email address
document.form1.text1.focus();
return true;
}
else
{
alert("You have entered an invalid email address!");    //The pop up alert for an invalid email address
document.form1.text1.focus();
return false;
}
}

</script>
<script language="JavaScript">
            var ev = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            var x = document.getElementById("check");
            function validate(email){
            if(!ev.test(email))
                {
                    x.innerHTML = "Not a valid email";
                    x.style.color = "red"
                }
            else
                {
                    x.innerHTML = "Looks good!";
                    x.style.color = "green"
                }
            }
        </script>
    <script type="text/javascript">
     function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('consultant_booking').style.display = 'block';
    }
    else document.getElementById('consultant_booking').style.display = 'none';

}
    </script>
    <script type="text/javascript">
      function myFunction() {
  // Get the checkbox
  var checkBox = document.getElementById("myCheck");
  // Get the output text
  var consultant_booking = document.getElementById("consultant_booking");

  // If the checkbox is checked, display the output text
  if (checkBox.checked == true){
    consultant_booking.style.display = "block";
  } else {
    consultant_booking.style.display = "none";
  }
}
    </script>

    <script type="text/javascript">
   function befsertime() {
    if (document.getElementById('befsertime').checked) {
        document.getElementById('ser_reqbox').style.display = 'block';
    }
    else document.getElementById('ser_reqbox').style.display = 'none';

}
    </script>  



<script type="text/javascript">
        $(document).ready(function(){
            $('#add_contact').on('click', function(){
                var count = $('#contact-area .contact-box').length;
                var new_count = count + 1;
                var html = '<div class="contact-box"><div class="form-group"><label for="" class="col-sm-2 control-label">Country Name<span style="color: red;">*</span></label><div class="col-sm-10"><input type="text" class="form-control" id="" placeholder="Country Name" name="contact_info['+new_count+'][country]" required value=""></div></div><div class="form-group"><label for="" class="col-sm-2 control-label">Address<span style="color: red;">*</span></label><div class="col-sm-10"><textarea type="text" rows="5" class="form-control" id="" placeholder="Description..." name="contact_info['+new_count+'][address]" required></textarea></div></div><div class="form-group"><label for="" class="col-sm-2 control-label">Email<span style="color: red;">*</span></label><div class="col-sm-10"><input type="text" class="form-control" id="" placeholder="Email" name="contact_info['+new_count+'][email]" required value=""></div></div><div class="form-group text-right"><button type="button" style="margin-right:15px;" class="btn btn-danger m-t-32 mr-2 remove"><i class="fa fa-trash-o"></i></button></div></div>';
                $('#contact-area').append(html);
            });
      $('.contact-area').on('click','.remove',function() {
          $(this).closest(".contact-box").remove();
          });
        });
    </script>
    <script type="text/javascript">

$(document).ready(function(){
$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
localStorage.setItem('activeTab', $(e.target).attr('href'));
});
var activeTab = localStorage.getItem('activeTab');
if(activeTab){
$('#myTab a[href="' + activeTab + '"]').tab('show');
}
});
</script>

    <script type="text/javascript">

$(document).ready(function(){
$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
localStorage.setItem('activeTab', $(e.target).attr('href'));
});
var activeTab = localStorage.getItem('activeTab');
if(activeTab){
$('#washtab a[href="' + activeTab + '"]').tab('show');
}
});
</script>
</body>
</html>