<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="{{route('dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-home text-aqua"></i> <span>Home</span></a>
           <ul class="treeview-menu">
            <li><a href="{{route('slider-banner')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Slider Banner</span></a></li>
            <li><a href="{{route('about')}}"><i class="fa fa-circle-o text-aqua"></i> <span>About</span></a></li>
            <li><a href="{{route('services')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Services</span></a></li>
            <li><a href="{{route('hr-solutions')}}"><i class="fa fa-circle-o text-aqua"></i> <span>HR Solutions</span></a></li>
        </ul>
        </li>
        <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-users text-aqua"></i> <span>About Us</span></a>
             <ul class="treeview-menu">
            <li><a href="{{route('who-we-are')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Who We Are</span></a></li>
            <li><a href="{{route('core-values')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Core Values</span></a></li>
            <li><a href="{{route('mission_vision')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Mission/Vision</span></a></li>
            <li><a href="{{route('leadership')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Leadership</span></a></li>
        </ul>
        </li>
        <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-gift text-aqua"></i> <span>Our Offering</span></a>
             <ul class="treeview-menu">
            <li><a href="{{route('it-managed-services')}}"><i class="fa fa-circle-o text-aqua"></i> <span>IT Managed Services</span></a></li>
            <li><a href="{{route('service-delivery')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Service Delivery</span></a></li>
            <li><a href="{{route('cloud-virtual')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Cloud & Virtualization</span></a></li>
            <li><a href="{{route('cyber-security')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Cyber Security</span></a></li>
            <li><a href="{{route('technology-management')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Technology Management</span></a></li>
            <li><a href="{{route('hardware-software-procurement')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Hardware & Software Procurement</span></a></li>
            <li><a href="{{route('asset-management')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Asset Management</span></a></li>
            <li><a href="{{route('human-resource-services')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Human Resource Recruitment Services</span></a></li>
            <li><a href="{{route('hr-advisory')}}"><i class="fa fa-circle-o text-aqua"></i> <span>HR Advisory Services</span></a></li>
        </ul>
        </li>
        <li><a href="{{route('partners')}}"><i class="fa fa-user text-aqua"></i> <span>Our Partners</span></a></li>
        <li><a href="{{route('customers')}}"><i class="fa fa-user text-aqua"></i> <span>Our Customers</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-book text-aqua"></i> <span>Case Study</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-picture-o text-aqua"></i> <span>Gallery</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-th text-aqua"></i> <span>Blogs</span></a></li>
        <li><a href="{{route('career')}}"><i class="fa fa-id-card-o text-aqua"></i> <span>Career</span></a></li>
        <li><a href="{{route('contact')}}"><i class="fa fa-id-card-o text-aqua"></i> <span>Contact</span></a></li>
        <li><a href="{{route('banners')}}"><i class="fa fa-id-card-o text-aqua"></i> <span>Inner Banner</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-globe text-aqua"></i> <span>Map</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-whatsapp text-aqua"></i> <span>Whats app chat integration</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-facebook-square text-aqua"></i> <span>Social Media</span></a></li>
        <li><a href="{{route('settings')}}"><i class="fa fa-user text-aqua"></i> <span>Settings</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
