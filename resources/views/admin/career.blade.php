@include('admin/layout/header')
@include('admin/layout/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Career
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Career</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Update Career</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('save-career-overview')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Title 1<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$overview->id??''}}">
                    <input type="text" class="form-control" id="" placeholder="Main Title..." name="title1" required value="{{$overview->title1??''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Title 1 description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control" id="editor1" placeholder="Description..." name="description1" required>{{$overview->title1_desc??''}}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Title 2<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Sub Title..." name="title2" required value="{{$overview->title2??''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Title 2 description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control" id="editor2" placeholder="Description..." name="description2" required>{{$overview->title2_desc??''}}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Add Job</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('add-job')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Job Title<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Main Title..." name="title" required value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Job description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control" id="editor1" placeholder="Description..." name="description" required></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Job List</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Job Title</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  @if(count($jobs) > 0)
                  @foreach($jobs as $job)
                  <td>{{$job->id}}</td>
                  <td>{{$job->job_title}}</td>
                  <td>
                    {{$job->description}}
                  </td>
                  <td>
                    @if($job->status == 1)
                    <button class="btn btn-success btn-xs">Active</button>
                    @else
                    <button class="btn btn-danger btn-xs">Deactive</button>
                    @endif
                  </td>
                  <td>
                    <div style="width: 140px;">
                    <button type="button" onclick="editJob({{$job->id}})" class="btn btn-primary btn-xs edit_model" data-toggle="modal" data-target="#edit_job"><i class="fa fa-edit"></i></button>

                    <button type="button" href="" onclick="deleteJob({{$job->id}})" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                   @if($job->status == 1)
                    <button class="btn btn-danger btn-xs" onclick="chnageStatus({{$job->id}})">Deactive</button>
                    @else
                    <button class="btn btn-success btn-xs" onclick="chnageStatus({{$job->id}})">Active</button>
                    @endif
                  </a>
                </div>
                  </td>
                </tr>    
                @endforeach
                @endif          
              </tbody>
              </table>
            </div>
            </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 <!-- edit modal -->
<div class="modal fade" id="edit_job" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Job</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" action="{{route('update-job')}}" method="post" id="edit_model">
          @csrf
             <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Job Title<span style="color: red;">*</span></label>

                  <div class="col-sm-9">
                    <input type="hidden" name="id" value="" id="job_id">
                    <input type="text" class="form-control" id="job_title" placeholder="Categories Name"  name="job_title" value="IT Infrastructure Managed Services" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Job Description<span style="color: red;">*</span></label>

                  <div class="col-sm-9">
                     <textarea type="text" rows="5" class="form-control" id="desc1" placeholder="Description..." name="description" required></textarea>
                  </div>
                </div>
              </div>
              <!-- /.
              /.box-body -->
              <div class="box-footer text-right">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>
</div>
 
@include('admin/layout/footer')
<script type="text/javascript">
    function chnageStatus(id){
      var token='{{csrf_token()}}';
      var job_id = id;
      $.ajax({
        url: "{{action('Admin\CareerController@changeJobStatus')}}",
        method: "POST",
        dataType: "json",
        data:{'id':job_id, '_token':token},
        success: function(response) {
          if(response.status == 200){
            alert('Status updated successfully !');
            location.reload(true);
          }
        }
      }); 
    }

    function editJob(id){
      var token='{{csrf_token()}}';
      var job_id = id;
      $.ajax({
        url: "{{action('Admin\CareerController@editJob')}}",
        method: "POST",
        dataType: "json",
        data:{'id':job_id, '_token':token},
        success: function(response) {
          if(response.status == 200){
            $('#job_id').val(job_id);
            $('#desc1').val(response.desc);
            $('#job_title').val(response.title);
          }
        }
      }); 
    }

    function deleteJob(id){
      var status = confirm('Are you sure..?');
      if(status == true){
        var token='{{csrf_token()}}';
        var job_id = id;
        $.ajax({
          url: "{{action('Admin\CareerController@deleteJob')}}",
          method: "POST",
          dataType: "json",
          data:{'id':job_id, '_token':token},
          success: function(response) {
            if(response.status == 200){
              alert('Job deleted successfully !');
              location.reload(true);
            }
          }
        }) 
      }else{
        return false
      }
    }

  
</script>