  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Home Banner</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" action="{{ action('AdminController\WebsiteHomeController@bannerEdit',[$homeBanner->id]) }}" method="post" id="edit_model">
          @csrf
              <div class="box-body">
                <!-- <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Banner Title</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" placeholder="Banner Title" onkeypress="return alphaonly(event)" name="title" required value="{{ $homeBanner->title ?? '' }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Banner Description</label>

                  <div class="col-sm-9">
                    <textarea type="text" rows="5" class="form-control" id="" onkeypress="return alphaonly(event)" placeholder="Banner Description" name="description" required>{{ $homeBanner->description ?? '' }}</textarea>
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Banner Image<span style="color: red">*</span></label>

                  <div class="col-sm-9">
                    <input type="file" name="banner_name" class="form-control" id="" onchange="fileValidation(this)" accept="image/jpg, image/jpeg, image/png" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label"></label>

                  <div class="col-sm-9">
                    
                     <img id="output_image" src="{{ asset($homeBanner->banner_path ?? 'admin/dist/img/placeholder/download.png') }}" style="width: 200px;height: 200px;"/>
                  </div>
                </div>
              </div>
              <!-- /.
              /.box-body -->
              <div class="box-footer text-right">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>