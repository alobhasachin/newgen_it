@include('admin/layout/header')
@include('admin/layout/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Settings
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Settings</li>
      </ol>
    </section>
    @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
    <!-- Main content -->
    <section class="content">
      <div class="row">
         <div class="col-md-12">
          <div class="box">
            <form class="form-horizontal" action="{{route('save-settings')}}" id="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-header with-border">
              <h3 class="box-title">Update Settings</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
      
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-9">
          <form class="form-horizontal" action="" id="add_form" method="post" enctype="multipart/form-data">
 
              <div class="box-body">
                <input type="hidden" name="id" value="{{$settings->id??''}}">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Website Logo<span style="color: red">*</span></label>

                  <div class="col-sm-9">
                    <input type="file" class="form-control" name="website_logo" @if(empty($settings->website_logo)) required @endif>
                    @if(!empty($settings->website_logo)) 
                    <img width="200" src="{{url($settings->website_logo)}}" class="mt-3">
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Admin Logo<span style="color: red">*</span></label>

                  <div class="col-sm-9">
                    <input type="file" class="form-control" name="admin_logo" @if(empty($settings->admin_logo)) required="" @endif>
                    @if(!empty($settings->admin_logo)) 
                    <img width="200" src="{{url($settings->admin_logo)}}" class="mt-3">
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Fevicon Icon<span style="color: red">*</span></label>

                  <div class="col-sm-9">
                    <input type="file" class="form-control" name="favicon_icon" @if(empty($settings->favicon_icon)) required="" @endif>
                    @if(!empty($settings->favicon_icon)) 
                    <img width="200" src="{{url($settings->favicon_icon)}}" class="mt-3">
                    @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Website Summary<span style="color: red">*</span></label>

                  <div class="col-sm-9">
                    <textarea type="text" rows="5" class="form-control" id="" placeholder="Description..." name="website_footer_summary" required>{{$settings->website_footer_summary??''}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Copyright Summary<span style="color: red">*</span></label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" name="copyright_summary" value="{{$settings->copyright_summary??''}}" required>
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              
            
        </div>
        <div class="col-md-1"></div>
      </div>

      <!-- /.box-body -->

            </form>

      </div>
      </div>
      </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 
@include('admin/layout/footer')