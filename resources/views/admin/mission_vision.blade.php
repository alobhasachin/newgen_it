@include('admin/layout/header')
@include('admin/layout/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       About Us
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">About</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
      
      <div class="row">
         <div class="col-md-12">
          <div class="box">
            <form class="form-horizontal" action="{{route('save-mission-vision')}}" id="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-header with-border">
              <h3 class="box-title">Update About Us</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
      
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Mission<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control" id="" placeholder="Description..." name="mission" required>{{$details->mission??''}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Vision<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control" id="" placeholder="Description..." name="vision" required>{{$details->vision??''}}</textarea>
                  </div>
                </div>
              </div>
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              
        </div>
        <div class="col-md-2"></div>
      </div>

      <!-- /.box-body -->
              
            </form>

      </div>
      </div>
      </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 

@include('admin/layout/footer')