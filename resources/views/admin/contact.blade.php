@include('admin/layout/header')
@include('admin/layout/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Contact Us
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
      <div class="row">
         <div class="col-md-12">
          <div class="box">
            <form class="form-horizontal" action="{{route('save-contact-details')}}" id="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-header with-border">
              <h3 class="box-title">Update Contact Us</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
      
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Contact Image<span style="color: red">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="file" name="contact_image" class="form-control" id="file" @if(empty($details->contact_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label"></label>
                  @if(!empty($details->contact_image))
                  <div class="col-sm-9">
                    
                     <img id="output_image" src="{{url($details->contact_image)}}" style="width: 200px;height: 200px;"/>
                  </div>
                  @endif
                </div>
                @php
                if(!empty($details->contact_info)){
                  $addresses = json_decode($details->contact_info);
                }else{
                  $addresses = [];

                }
                @endphp
                @if(empty($addresses) && count($addresses) <= 0)
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Country Name<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Country Name" name="contact_info[0][country]" required value="{{!empty($addresses)?$addresses[0]->country:''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Address<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control" id="" placeholder="Description..." name="contact_info[0][address]" required>{{!empty($addresses)?$addresses[0]->address:''}}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Email<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Email" name="contact_info[0][email]" required value="{{!empty($addresses)?$addresses[0]->email:''}}">
                  </div>
                </div>
                @endif
                @php $i = 0; @endphp
                <div id="contact-area" class="contact-area">
                @if(!empty($addresses))
                @foreach($addresses as $address)
                <div class="contact-box">
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Country Name<span style="color: red;">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="" placeholder="Country Name" name="contact_info[{{$i}}][country]" required value="{{$address->country}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Address<span style="color: red;">*</span></label>
                    <div class="col-sm-10">
                      <textarea type="text" rows="5" class="form-control" id="" placeholder="Description..." name="contact_info[{{$i}}][address]" required>{{$address->address}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Email<span style="color: red;">*</span>
                    </label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="" placeholder="Email" name="contact_info[{{$i}}][email]" required value="{{$address->email}}">
                    </div>
                  </div>
                  @if($i != 0)
                  <div class="form-group text-right"><button type="button" style="margin-right:15px;" class="btn btn-danger m-t-32 mr-2 remove"><i class="fa fa-trash-o"></i></button>
                  </div>
                  @endif
                </div>
                @php $i++; @endphp
                @endforeach
                @endif    
              
              </div>
              <div class="form-group text-right">
              <button type="button" class="btn btn-success" style="margin-right:15px;" id="add_contact">Add More</button>
              </div>
              </div>
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
              
        </div>
        <div class="col-md-2"></div>
      </div>

      <!-- /.box-body -->
              
            </form>

      </div>
      </div>
      </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 

@include('admin/layout/footer')