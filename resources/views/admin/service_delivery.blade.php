@include('admin/layout/header')
@include('admin/layout/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Service Delivery
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Service Delivery</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Service Delivery</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-service-delivery')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Main Title<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="text" class="form-control" id="" placeholder="Main Title..." name="main_title" required value="{{$details->main_title??''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->description??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->image)}}" style="width: 300px;height: auto;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">24X7 Remote Support</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-remote-support')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->support_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

          <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">End-User & Onsite Technical Support</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-technical-support')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Title 1<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="text" class="form-control" id="" placeholder="Main Title..." name="title1" required value="{{$details->tech_support_title1??''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description1" required>{!!$details->tech_support_desc1??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Title 2<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Main Title..." name="title2" required value="{{$details->tech_support_title2??''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description2" required>{!!$details->tech_support_desc2??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->tech_support_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->tech_support_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->tech_support_image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Last Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="last_description" required>{!!$details->tech_support_last_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Staff Augmentation</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-staff-augmentation')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="file" name="image" class="form-control" id="file"  @if(empty($details->staff_image)) required @endif  accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->staff_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->staff_image)}}" style="width: 300px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->staff_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Desktop Support</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-desktop-support')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->desktop_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->desktop_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->desktop_image)}}" style="width: 300px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->desktop_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Industry Best Practices</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-best-practices')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->best_practices??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 
@include('admin/layout/footer')