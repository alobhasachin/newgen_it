@include('admin/layout/header')
@include('admin/layout/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Customers
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customers</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
    <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Add Customer</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <div class="box-body">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <form class="form-horizontal" action="{{route('add-customer')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Customer Image<span style="color: red">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="customer_image" class="form-control" id="file" required accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-1"></div>
      </div>
    </div>
      </div>
      </div>
      </div>
      <!-- /.row -->
      <div class="row">
         <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Customer List</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
        
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th width="300">Customer Image</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @if(!empty($customers))
                  @foreach($customers as $customer)
                <tr>
                  <td>{{$customer->id}}</td>
                  <td>
                    @if(!empty($customer->customer_image))
                    <img src="{{url($customer->customer_image)}}" style="width: 100px; ">
                    @endif
                  </td>
                  <th>
                    @if($customer->status == 1)
                    <button class="btn btn-success btn-xs">Active</button>
                    @else
                    <button class="btn btn-danger btn-xs">Deactive</button>
                    @endif
                  </th>
                  <td>
                    <div style="width: 140px;">
                    <button type="button" href="#" onclick="editCustomer({{$customer->id}})" class="btn btn-primary btn-xs edit_model" data-toggle="modal" data-target="#edit_customer"><i class="fa fa-edit"></i></button>

                    <button type="button" onclick="deleteCustomer({{$customer->id}})" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    <a type="button"> 
                     @if($customer->status == 1)
                    <button class="btn btn-danger btn-xs" onclick="chnageStatus({{$customer->id}})">Deactive</button>
                    @else
                    <button class="btn btn-success btn-xs" onclick="chnageStatus({{$customer->id}})">Active</button>
                    @endif
                    </a>
                  </div>
                  </td>
                </tr>   
                @endforeach
                @endif          
              </tbody>
              </table>
            </div>
            </div>
            </div>
            </div>
            </div>
            <!-- /.box-body -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- edit modal -->
<div class="modal fade" id="edit_customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Customer</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" action="{{route('update-customer')}}" method="post" id="edit_model"enctype="multipart/form-data">
          @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Customer Image<span style="color: red">*</span></label>

                  <div class="col-sm-9">
                    <input type="hidden" name="customer_id" id="customer_id">
                    <input type="file" name="customer_image" class="form-control" id="" onchange="fileValidation(this)" accept="image/jpg, image/jpeg, image/png" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label"></label>

                  <div class="col-sm-9" id="customer_image">
                    
                     <img id="output_image" src="dist/img/1.png" style="width: 200px;height: 200px;"/>
                  </div>
                </div>
              </div>
              <!-- /.
              /.box-body -->
              <div class="box-footer text-right">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>
</div>

@include('admin/layout/footer')
<script type="text/javascript">
    function chnageStatus(id){
      var token='{{csrf_token()}}';
      var customer_id = id;
      $.ajax({
        url: "{{action('Admin\CustomerController@changeCustomerStatus')}}",
        method: "POST",
        dataType: "json",
        data:{'id':customer_id, '_token':token},
        success: function(response) {
          if(response.status == 200){
            alert('Status updated successfully !');
            location.reload(true);
          }
        }
      }); 
    }

    function editCustomer(id){
      var token='{{csrf_token()}}';
      var customer_id = id;
      $.ajax({
        url: "{{action('Admin\CustomerController@editCustomer')}}",
        method: "POST",
        dataType: "json",
        data:{'id':customer_id, '_token':token},
        success: function(response) {
          if(response.status == 200){
            $('#customer_id').val(customer_id);
            var image_url = '{{url('/')}}/'+response.img;
            $('#customer_image').html('<img id="output_image" src="'+image_url+'" style="width: 200px;height: 200px;"/>');
          }
        }
      }); 
    }

    function deleteCustomer(id){
      var status = confirm('Are you sure..?');
      if(status == true){
        var token='{{csrf_token()}}';
        var customer_id = id;
        $.ajax({
          url: "{{action('Admin\CustomerController@deleteCustomer')}}",
          method: "POST",
          dataType: "json",
          data:{'id':customer_id, '_token':token},
          success: function(response) {
            if(response.status == 200){
              alert('Customer deleted successfully !');
              location.reload(true);
            }
          }
        }) 
      }else{
        return false
      }
    }

  
</script>