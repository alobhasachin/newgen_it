@include('admin/layout/header')
@include('admin/layout/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Banner
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Banner</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
    <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">About Banner</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <div class="box-body">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('save-about-banner')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Banner Image<span style="color: red">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$banners->id??''}}">
                    <input type="file" name="about_banner" class="form-control" id="file" @if(empty($banners->about_banner)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($banners->about_banner))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($banners->about_banner)}}" style="width: 200px;height: 200px;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
      </div>
      </div>
      </div>
      <!-- /.row -->
    
    <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Partner Banner</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <div class="box-body">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('save-partner-banner')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Banner Image<span style="color: red">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$banners->id??''}}">
                    <input type="file" name="partner_banner" class="form-control" id="file" @if(empty($banners->partner_banner)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>

                  @if(!empty($banners->partner_banner))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($banners->partner_banner)}}" style="width: 200px;height: 200px;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
      </div>
      </div>
      </div>
      <!-- /.row -->

    <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Customer Banner</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <div class="box-body">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('save-customer-banner')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Banner Image<span style="color: red">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$banners->id??''}}">
                    <input type="file" name="customer_banner" class="form-control" id="file" @if(empty($banners->customer_banner)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>

                  @if(!empty($banners->customer_banner))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($banners->customer_banner)}}" style="width: 200px;height: 200px;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
      </div>
      </div>
      </div>
      <!-- /.row -->

    <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Career Banner</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <div class="box-body">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('save-career-banner')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Banner Image<span style="color: red">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$banners->id??''}}">
                    <input type="file" name="career_banner" class="form-control" id="file" @if(empty($banners->career_banner)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>

                  @if(!empty($banners->career_banner))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($banners->career_banner)}}" style="width: 200px;height: 200px;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
      </div>
      </div>
      </div>
      <!-- /.row -->

  <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Contact Banner</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <div class="box-body">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('save-contact-banner')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Banner Image<span style="color: red">*</span></label>

                  <div class="col-sm-10">

                    <input type="hidden" name="id" value="{{$banners->id??''}}">
                    <input type="file" name="contact_banner" class="form-control" id="file" @if(empty($banners->contact_banner)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>

                  @if(!empty($banners->contact_banner))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($banners->contact_banner)}}" style="width: 200px;height: 200px;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
      </div>
      </div>
      </div>
      <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@include('admin/layout/footer')