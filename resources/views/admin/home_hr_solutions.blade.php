@include('admin/layout/header')
@include('admin/layout/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       HR Solutions
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">HR Solutions</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
      <div class="row">
         <div class="col-md-12">
          <div class="box">
            <form class="form-horizontal" action="{{route('save-hr-solutions')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="box-header with-border">
              <h3 class="box-title">Update HR Solutions</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
      
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          
              <div class="box-body">
                <input type="hidden" name="id" value="{{$solution->id??''}}">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Main Title<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Main Title..." name="title" required value="{{$solution->main_title??''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Main description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control" id="editor1" placeholder="Description..." name="description" required>{{$solution->main_description??''}}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub Title<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="" placeholder="Sub Title..." name="sub_title" required value="{{$solution->sub_title??''}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Sub description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control" id="editor2" placeholder="Description..." name="sub_description" required>{{$solution->sub_description??''}}</textarea>
                  </div>
                </div>
              </div>
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
        </div>
        <div class="col-md-2"></div>
      </div>

      <!-- /.box-body -->
              
            </form>

      </div>
      </div>
      </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 

@include('admin/layout/footer')