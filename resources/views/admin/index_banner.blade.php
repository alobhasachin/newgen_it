@include('admin/layout/header')
@include('admin/layout/sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Banner
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Banner</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
    <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Add Banner</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <div class="box-body">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <form class="form-horizontal" action="{{route('add-slider-banner')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Banner Image<span style="color: red">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="banner_image" class="form-control" id="file" required accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>

                  <div class="col-sm-10">
                    
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
      </div>
      </div>
      </div>
      <!-- /.row -->
      <div class="row">
         <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Home Banner</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
        
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th width="300">Banner Image</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($banners))  
                @foreach($banners as $banner)
                <tr>
                  <td>{{$banner->id}}</td>
                  <td>
                    @if(!empty($banner->banner_image))
                    <img src="{{url($banner->banner_image)}}" style="width: 100px; ">
                    @endif
                  </td>
                  <th>
                    @if($banner->status == 1)
                    <button class="btn btn-success btn-xs">Active</button>
                    @else
                    <button class="btn btn-danger btn-xs">Deactive</button>
                    @endif
                  </th>
                  <td>
                    <div style="width: 140px;">
                    <button type="button" href="#" onclick="editBanner({{$banner->id}})" class="btn btn-primary btn-xs edit_model" data-toggle="modal" data-target="#edit_banner"><i class="fa fa-edit"></i></button>

                    <button type="button" onclick="deleteBanner({{$banner->id}})" class="btn btn-danger btn-xs" id="delete"><i class="fa fa-trash-o"></i></button>
                    <a type="button"> 
                    @if($banner->status == 1)
                    <button class="btn btn-danger btn-xs" onclick="chnageStatus({{$banner->id}})">Deactive</button>
                    @else
                    <button class="btn btn-success btn-xs" onclick="chnageStatus({{$banner->id}})">Active</button>
                    @endif
                    </a>
                  </div>
                  </td>
                </tr>  
                @endforeach
                @endif           
              </tbody>
              </table>
            </div>
            </div>
            </div>
            </div>
            </div>
            <!-- /.box-body -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- edit modal -->
<div class="modal fade" id="edit_banner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Home Banner</h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" action="{{route('update-slider-banner')}}" method="post" id="edit_model" enctype="multipart/form-data">
          @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Banner Image<span style="color: red">*</span></label>

                  <div class="col-sm-9">
                    <input type="hidden" name="banner_id" id="banner_id">
                    <input type="file" name="banner_image" class="form-control" id="" onchange="fileValidation(this)" accept="image/jpg, image/jpeg, image/png" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label"></label>

                  <div class="col-sm-9" id="old_banner_image">
                    <img id="output_image" src="dist/img/banner-1.png" style="width: 200px;height: 200px;"/>
                  </div>
                </div>
              </div>
              <!-- /.
              /.box-body -->
              <div class="box-footer text-right">
                <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-main">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
      </div>
    </div>

  </div>
</div>

@include('admin/layout/footer')
<script type="text/javascript">
    function chnageStatus(id){
      var token='{{csrf_token()}}';
      var banner_id = id;
      $.ajax({
        url: "{{action('Admin\AdminController@changeBannerStatus')}}",
        method: "POST",
        dataType: "json",
        data:{'id':banner_id, '_token':token},
        success: function(response) {
          if(response.status == 200){
            alert('Status updated successfully !');
            location.reload(true);
          }
        }
      }); 
    }

    function editBanner(id){
      var token='{{csrf_token()}}';
      var banner_id = id;
      $.ajax({
        url: "{{action('Admin\AdminController@editBanner')}}",
        method: "POST",
        dataType: "json",
        data:{'id':banner_id, '_token':token},
        success: function(response) {
          if(response.status == 200){
            $('#banner_id').val(banner_id);
            var image_url = '{{url('/')}}/'+response.img;
            $('#old_banner_image').html('<img id="output_image" src="'+image_url+'" style="width: 200px;height: 200px;"/>');
          }
        }
      }); 
    }

    function deleteBanner(id){
      var status = confirm('Are you sure..?');
      if(status == true){
        var token='{{csrf_token()}}';
        var banner_id = id;
        $.ajax({
          url: "{{action('Admin\AdminController@deleteBanner')}}",
          method: "POST",
          dataType: "json",
          data:{'id':banner_id, '_token':token},
          success: function(response) {
            if(response.status == 200){
              alert('Banner deleted successfully !');
              location.reload(true);
            }
          }
        }) 
      }else{
        return false
      }
    }

  
</script>