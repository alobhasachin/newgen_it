@include('admin/layout/header')
@include('admin/layout/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Cloud & Virtualization
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Cloud & Virtualization</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Cloud & Virtualization</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
      
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-cloud-virtulization')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->desription??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Cloud</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-cloud-description')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->cloud_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

          <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">CLOUD SOLUTIONS</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-cloud-solutions')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description1" required>{!!$details->cloud_sol_desc??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->cloud_sol_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->cloud_sol_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->cloud_sol_image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Hybrid Solutions Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description2" required>{!!$details->hybrid_sol_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

         <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Virtualization & Server Virtualization</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-server-virtulization')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Virtualization Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description1" required>{!!$details->virtual_desc??''!!}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Server Virtualization Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description2" required>{!!$details->server_virtual_desc??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->virtual_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->virtual_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->virtual_image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Storage Virtualization</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-storage-virtulization')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->storage_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->storage_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->storage_image)}}" style="width: 300px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->storage_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Desktop Virtualization & High Availability</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-desktop-virtulization')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->desktop_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->desktop_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->desktop_image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Desktop Virtualization Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description1" required>{!!$details->desktop_virtual_desc??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">High Availability Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description2" required>{!!$details->high_avail_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Disaster Recovery & Hosting</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-disaster-recovery')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Disaster Recovery Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description1" required>{!!$details->disaster_desc??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->disaster_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->disaster_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->disaster_image)}}" style="width: 300px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Hosting Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description2" required>{!!$details->hosting_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

         <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Voice Solutions & Software As A Service</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-voice-solutions')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Voice Solutions Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description1" required>{!!$details->voice_desc??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Software As A Service Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description2" required>{!!$details->software_desc??''!!}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->voice_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->voice_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->voice_image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Infrastructure As A Service</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-infra-desc')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{{$details->infrastructure_desc??''}}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 
@include('admin/layout/footer')