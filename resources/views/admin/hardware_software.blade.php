@include('admin/layout/header')
@include('admin/layout/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Hardware & Software Procurement
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Hardware & Software Procurement</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Hardware & Software Procurement</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-hardware-proc')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Hardware & Software Procurement Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description1" required>{!!$details->hw_sw_procurement_desc??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Full Range Of Products Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description2" required>{!!$details->full_range_desc??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Infrastructure Equipment</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-infra-equipment')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->equipment_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>
       <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Infrastructure Software</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-infra-software')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->software_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>
         <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">End User Equipment</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-end-user-equipment')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->end_user_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>
         <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Other Equipment</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-other-equipment')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->other_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 @include('admin/layout/footer')