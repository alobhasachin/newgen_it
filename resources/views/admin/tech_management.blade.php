@include('admin/layout/header')
@include('admin/layout/sidebar')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Technology Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Technology Management</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @if($errors->any())
          @foreach ($errors->all() as $error)
              <div style="color:red;margin-left: 20px;">{{ $error }}</div>
          @endforeach
      @endif
        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Infrastructure Management</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-infra-mgmt')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">

                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->infra_mgmt_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->infra_mgmt_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->infra_mgmt_image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->infra_mgmt_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Network And System Management Services</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-network-service')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>
                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->network_service_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

          <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Network Management</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-network-mgmt')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->network_mgmt_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->network_mgmt_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->network_mgmt_image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->network_mgmt_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

         <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Network And System Management Outsourcing Benefits</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-outsource-benefits')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description1" required>{!!$details->benefits_desc??''!!}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control" id="file" @if(empty($details->benefits_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->benefits_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->benefits_image)}}" style="width: 300px;height: auto;"/>
                  </div>
                  @endif
                </div>
                 <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Backup Solutions Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description2" required>{!!$details->backup_sol_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

         <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Application Management And Database Administration</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-app-mgmt')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Image<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <input type="file" name="image" class="form-control" id="file"  @if(empty($details->app_image)) required @endif accept="image/jpg, image/jpeg, image/png"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label"></label>
                  @if(!empty($details->app_image))
                  <div class="col-sm-10">
                    <img id="output_image" src="{{url($details->app_image)}}" style="width: 200px;height: auto;"/>
                  </div>
                  @endif
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->app_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>


        <div class="row">
         <div class="col-md-12">
          <div class="box collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Monitoring And Alerting</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
           
      <!-- Info boxes -->
      <div class="row">
        <!-- <div class="col-md-2"></div> -->
        <div class="col-md-12">
          <form class="form-horizontal" action="{{route('add-monitoring-desc')}}" id="add_form" method="post" enctype="multipart/form-data">
            @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Description<span style="color: red;">*</span></label>

                  <div class="col-sm-10">
                    <input type="hidden" name="id" value="{{$details->id??''}}">
                    <textarea type="text" rows="5" class="form-control editor" id="" placeholder="Description..." name="description" required>{!!$details->monitoring_desc??''!!}</textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-right">
                <button type="reset" class="btn btn-default">Reset</button>
                <button type="submit" class="btn btn-main">Submit</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- <div class="col-md-2"></div> -->
      </div>
      
 </div>
              <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        </div>

  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('admin/layout/footer')