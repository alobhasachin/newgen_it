<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'frontend';

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    # Get home page view
    public function home()
    {   
        $slider_banners = \App\Models\SliderBanner::where('status', '1')->get();
        $about_us = \App\Models\AboutUs::first();
        $Hr_solutions = \App\Models\HrSolution::first();
        $services = \App\Models\Service::where('status', '1')->get();
        $contact_details = \App\Models\Contact::first();
        return view($this->view.'.index')->with(['slider_banners' => $slider_banners, 'about_us' => $about_us, 'Hr_solutions' => $Hr_solutions, 'services' => $services, 'contact_details' => $contact_details]);
    }

    # Get About Us page view
    public function aboutUs()
    {
        $details = \App\Models\MainAboutUs::first();
        $banners = \App\Models\Banner::all();
        return view($this->view.'.about')->with(['details' => $details, 'banners' => $banners]);
    }

    # Get Career page view
    public function Career()
    {
        $career_details = \App\Models\CareerOverview::first();
        $jobs = \App\Models\Job::where('status', '1')->get();
        $banners = \App\Models\Banner::all();
        return view($this->view.'.career')->with(['career_details' => $career_details, 'jobs' => $jobs, 'banners' => $banners]);
    }

    # Get Contact page view
    public function Contact()
    {
        $contact_details = \App\Models\Contact::first();
        $banners = \App\Models\Banner::all();
        return view($this->view.'.contact')->with(['contact_details' => $contact_details, 'banners' => $banners]);
    }

    # Get Customers page view
    public function Customers()
    {
        $customers = \App\Models\Customer::where('status', '1')->get();
        $banners = \App\Models\Banner::all();
        return view($this->view.'.customer_base')->with(['customers' => $customers, 'banners' => $banners]);
    }

    # Get Partners page view
    public function Partners()
    {   
        $partners = \App\Models\Partner::where('status', '1')->get();
        $banners = \App\Models\Banner::all();
        return view($this->view.'.partner')->with(['partners' => $partners, 'banners' => $banners]);
    }

    
}
