<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use App\Models\MainAboutUs;

class AboutUsController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'admin';

    # Bind the Main about us model
    protected $main_about_us;

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MainAboutUs $main_about_us)
    {
        $this->middleware('guest');
        $this->main_about_us  = $main_about_us;
     }

    # function to Who we are
    public function whoWeAre()
    {
        $details = $this->main_about_us::first();
        return view($this->view.'.whoweare')->with(['details' => $details]);
    }

    # function to save or update Who we are
    public function saveWhoWeAre(Request $request)
    {
        $input = $request->all();

        # validate the request data
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
            'main_image' => 'required',
            ]);
        }

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $upadted_data = [
            'title' => $input['title']??'',
            'main_description' => $input['description']??'',
        ];

        if(!empty($input['main_image']) && $input['main_image'] != null){
            if($request->hasfile('main_image')){
                $file = $request->file('main_image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $main_image='img/'.$filename;
            }
            $upadted_data['main_image'] = $main_image;
        }

        if(!empty($input['id']) && $input['id'] > '0'){
            $data = $this->main_about_us::find($input['id']);
            $data->update($upadted_data);
        }else{
            $data = $this->main_about_us::create($upadted_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Detail updated.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }

    }

    # function to Leadership
    public function leadership()
    {
        $details = $this->main_about_us::first();
        return view($this->view.'.leadership')->with(['details' => $details]);
    }

    # save leadership
    public function saveLeadership(Request $request)
    {
        $input = $request->all();
        # validate the request data
        $validation = Validator::make($request->all(), [
            'description' => 'required',
            
        ]);

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
            'image' => 'required',
            ]);
        }

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'description2' => $input['description']??''
        ];

        if(!empty($input['image']) && $input['image'] != null){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
            }
            $update_data['image'] = $image;
        }

        if(!empty($input['id']) && $input['id'] > '0'){
            $data = $this->main_about_us::find($input['id']);
            $data->update($update_data);
        }else{
            $data = $this->main_about_us::create($update_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Detail updated.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }


    # function to Core values
    public function coreValues()
    {
        $details = $this->main_about_us::first();
        return view($this->view.'.corevalue')->with(['details' => $details]);
    }

    # Save core values
    public function saveCoreValues(Request $request)
    {
        $input = $request->all();

        # validate the request data
        $validation = Validator::make($request->all(), [
            'core_value' => 'required',
            'teamwork' => 'required',
            'spirit' => 'required',
            'responsibility' => 'required',
            'focus' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'core_value' => $input['core_value']??'',
            'teamwork'   => $input['teamwork']??'',
            'spirit'     => $input['spirit']??'',
            'responsibility' => $input['responsibility']??'',
            'focus' => $input['focus']??'',
        ];

        if(isset($input['id']) && $input['id'] > '0'){
            $data = $this->main_about_us::find($input['id']);
            $data->update($update_data);

        }else{
            $data = $this->main_about_us::create($update_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Detail updated.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }
    
     # function to Mission vision
    public function missionVision()
    {
        $details = $this->main_about_us::first();
        return view($this->view.'.mission_vision')->with(['details' => $details]);
    }    

    # Function to save mission vision
    public function saveMissionVision(Request $request)
    {
        $input = $request->all();

        # validate the request data
        $validation = Validator::make($request->all(), [
            'mission' => 'required',
            'vision' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'mission' => $input['mission']??'',
            'vision'  => $input['vision']??'',
        ];

        if(isset($input['id']) && $input['id'] > '0'){
            $data = $this->main_about_us::find($input['id']);
            $data->update($update_data);

        }else{
            $data = $this->main_about_us::create($update_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Detail updated.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }
}
