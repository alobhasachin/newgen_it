<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use App\Models\SliderBanner;
use App\Models\Service;
use App\Models\AboutUs;
use App\Models\HrSolution;
use App\Models\Setting;

class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'admin';

    # Bind the slider banner model
    protected $slider_banner;

    # Bind the Service model
    protected $service;

    # Bind the About us model
    protected $about_us;

    # Bind the Hr Solution Model
    protected $hr_solution;

    # Bind Settings model
    protected $setting;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SliderBanner $slider_banner, Service $service, AboutUs $about_us, HrSolution $hr_solution, Setting $setting)
    {
        $this->middleware('guest');
        $this->slider_banner = $slider_banner;
        $this->service       = $service;
        $this->about_us      = $about_us;
        $this->hr_solution   = $hr_solution;
        $this->setting       = $setting;
    }

    /**
     *
     * Function to get dashboard
     *
     */
    public function dashboard()
    {
        return view($this->view.'.index');   
    }

    /**
     *
     * Function to Slider Banners
     *
     */
    public function sliderBanners()
    {
        $banners = $this->slider_banner::get();
        return view($this->view.'.index_banner')->with(['banners' => $banners]);   
    }

    # Add slider Banners
    public function addSliderBanner(Request $request)
    {
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'banner_image' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }
        if($request->hasfile('banner_image')){
            $file = $request->file('banner_image');
            $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
            $file->move('img/', $filename);
            $banner_image='img/'.$filename;
        }

        $data = [
            'banner_image' => $banner_image,
            'status'       => '1',
        ];

        $data = $this->slider_banner::create($data);
        if($data){
            return redirect()->back()->withErrors(['success' => 'Banner added successfully.']);
        }
    }

    # Uodate slider Banners
    public function updateSliderBanner(Request $request)
    {
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'banner_image' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }
        if($request->hasfile('banner_image')){
            $file = $request->file('banner_image');
            $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
            $file->move('img/', $filename);
            $banner_image='img/'.$filename;
        }

        $data = [
            'banner_image' => $banner_image,
            'status'       => '1',
        ];

        $data = $this->slider_banner::find($request->get('banner_id'));
        if($data){
            $data->update(['banner_image' => $banner_image]);
            return redirect()->back()->withErrors(['success' => 'Banner updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }   

    # Change Banner Status
    public function changeBannerStatus(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $banner = $this->slider_banner::find($input['id']);
            if($banner->status == '1'){
                $banner = $banner->where('id', $input['id'])->update(['status' => '0']);
            }else if($banner->status == '0'){
                $banner = $banner->where('id', $input['id'])->update(['status' => '1']);
            }

            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Delete banner
    public function deleteBanner(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $banner = $this->slider_banner::find($input['id']);
            $banner->delete();
            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Edit banner
    public function editBanner(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $banner = $this->slider_banner::find($input['id']);
            return $response = [
                'status' => 200,
                'img' => $banner->banner_image, 
            ];
         }
     } 

    /**
     *
     * Function to HR Solutions
     *
     */
    public function hrSolutions()
    {   
        $solution = $this->hr_solution::first();
        return view($this->view.'.home_hr_solutions')->with(['solution' => $solution]);   
    }

    # Save HR Solutions details
    public function saveHrSolutions(Request $request)
    {
        $input = $request->all();
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'sub_title' => 'required',
            'sub_description' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'main_title'  => $input['title']??'',
            'main_description' => $input['description']??'',
            'sub_title'       => $input['sub_title']??'',
            'sub_description'       => $input['sub_description']??'',
        ];

        if(isset($input['id']) && $input['id'] > '0'){
            $data = $this->hr_solution::find($input['id']);
            $data->update($update_data);
        }else{
            $data = $this->hr_solution::create($update_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Hr Solutions details updated !']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
        
    }

    /**
     *
     * Function to about
     *
     */
    public function about()
    {   
        $aboutDetails = $this->about_us::first();
        return view($this->view.'.home_about')->with(['aboutDetails' => $aboutDetails]);   
    }

    #save About details
    public function saveAboutDetails(Request $request)
    {
        $input = $request->all();
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
            'image' => 'required',
            ]);
        }

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
            }
            $update_data['image'] = $image;
        }

        

        if(isset($input['id']) && $input['id'] > '0'){
            $data = $this->about_us::find($input['id']);
            $data->update($update_data);
        }else{
            $data = $this->about_us::create($update_data);
        }    
        if($data){
            return redirect()->back()->withErrors(['success' => 'Details updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }


    /**
     *
     * Function to Services
     *
     */
    public function services()
    {   
        $services = $this->service::get();
        return view($this->view.'.home_service')->with(['services' => $services]);   
    }

    # add service
    public function addService(Request $request)
    {
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        if($request->hasfile('image')){
            $file = $request->file('image');
            $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
            $file->move('img/', $filename);
            $image='img/'.$filename;
        }

        $data = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'image'       => $image,
            'status'      => '1',
        ];

        $data = $this->service::create($data);
        if($data){
            return redirect()->back()->withErrors(['success' => 'Service added successfully.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
        
    }

    # Change Service Status
    public function changeServiceStatus(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $service = $this->service::find($input['id']);
            if($service->status == '1'){
                $service = $service->where('id', $input['id'])->update(['status' => '0']);
            }else if($service->status == '0'){
                $service = $service->where('id', $input['id'])->update(['status' => '1']);
            }

            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Edit Service
    public function editService(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $service = $this->service::find($input['id']);
            return $response = [
                'status' => 200,
                'img' => $service->image, 
                'name'  => $service->name,
                'desc'  => $service->description
            ];
         }
     } 

    # Service Delete
    public function deleteService(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $service = $this->service::find($input['id']);
            $service->delete();
            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Update the Service

     # add service
    public function updateService(Request $request)
    {
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'status'      => '1',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
            }
            $update_data['image'] = $image;
        }

        
        $data = $this->service::find($request->get('service_id'));
        if($data){
            $data->update($update_data);
            return redirect()->back()->withErrors(['success' => 'Service updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
        
    }

    /**
     *
     * Function to Settings
     *
     */
    public function settings()
    {
        $settings = $this->setting::first();
        return view($this->view.'.setting')->with(['settings' => $settings]);   
    }

    # Save Settings
    public function saveSettings(Request $request)
    {
        $input = $request->all();
        if(empty($input['id']) && $input['id'] == null){
            # validate the email and password
            $validation = Validator::make($request->all(), [
                'website_footer_summary' => 'required',
                'copyright_summary' => 'required',
                'website_logo' => 'required',
                'admin_logo' => 'required',
                'favicon_icon' => 'required',
            ]);

            if($validation->fails()) {  
                return redirect()->back()->withErrors($validation);
            }
        }


        $update_data = [
            'website_footer_summary'  => $input['website_footer_summary']??'',
            'copyright_summary' => $input['copyright_summary']??'',
        ];

        if(!empty($input['website_logo']) && $input['website_logo'] != null){
            if($request->hasfile('website_logo')){
                $file = $request->file('website_logo');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $website_logo='img/'.$filename;
            }
            $update_data['website_logo'] = $website_logo;
        }


        if(!empty($input['admin_logo']) && $input['admin_logo'] != null){
            if($request->hasfile('admin_logo')){
                $file = $request->file('admin_logo');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $admin_logo='img/'.$filename;
            }
            $update_data['admin_logo'] = $admin_logo;
        }


        if(!empty($input['favicon_icon']) && $input['favicon_icon'] != null){
            if($request->hasfile('favicon_icon')){
                $file = $request->file('favicon_icon');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $favicon_icon='img/'.$filename;
            }
            $update_data['favicon_icon'] = $favicon_icon;
        }
        
        

        if(isset($input['id']) && $input['id'] > '0'){
            $data = $this->setting::find($input['id']);
            $data->update($update_data);
        }else{
            $data = $this->setting::create($update_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Settings updated !']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
        
    }



}
