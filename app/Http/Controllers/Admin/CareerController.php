<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use App\Models\CareerOverview;
use App\Models\Job;

class CareerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'admin';

    # Bind the Job model
    protected $job;

    # Bind the Career Overview
    protected $career_overview;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Job $job, CareerOverview $career_overview)
    {
        $this->middleware('guest');
        $this->job = $job;
        $this->career_overview = $career_overview;
    }

    # Get Career view
    public function Career()
    {
        $overview = $this->career_overview::first();
        $jobs = $this->job::all();
        return view($this->view.'.career')->with(['overview' => $overview, 'jobs' => $jobs]);
    }
    

    # Save or Update career overview
    public function saveCareerOverview(Request $request)
    {
        $input = $request->all();
        # validate the request data
        $validation = Validator::make($request->all(), [
            'title1' => 'required',
            'description1' => 'required',
            'title2' => 'required',
            'description2' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'title1' => $input['title1']??'',
            'title1_desc' => $input['description1']??'',
            'title2' => $input['title2']??'',
            'title2_desc' => $input['description2']??'',
        ];

        if(!empty($input['id']) && $input['id'] > '0'){
            $data = $this->career_overview::find($input['id']);
            $data->update($update_data);
        }else{
            $data = $this->career_overview::create($update_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Detail updated.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }

    # Add job
    public function addJob(Request $request)
    {
        $input = $request->all();
       # validate the request data
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'job_title' => $input['title']??'',
            'description' => $input['description']??'',
            'status'     => '1',
        ];

        $data = $this->job::create($update_data);

        if($data){
            return redirect()->back()->withErrors(['success' => 'Job Added.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }

    # Update job
    public function updateJob(Request $request)
    {
        $input = $request->all();
       # validate the request data
        $validation = Validator::make($request->all(), [
            'job_title' => 'required',
            'description' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }

        $update_data = [
            'job_title' => $input['job_title']??'',
            'description' => $input['description']??'',
            'status'     => '1',
        ];

        if(isset($input['id']) && $input['id'] > '0'){
            $data = $this->job::find($input['id']);
            $data->update($update_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Job Updated.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }

    # Change Job Status
    public function changeJobStatus(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $job = $this->job::find($input['id']);
            if($job->status == '1'){
                $job = $job->where('id', $input['id'])->update(['status' => '0']);
            }else if($job->status == '0'){
                $job = $job->where('id', $input['id'])->update(['status' => '1']);
            }

            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Edit Job
    public function editJob(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $job = $this->job::find($input['id']);
            return $response = [
                'status' => 200,
                'title' => $job->job_title, 
                'desc'  => $job->description
            ];
         }
     } 

    # Service Delete
    public function deleteJob(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $job = $this->job::find($input['id']);
            $job->delete();
            return $response = [
                'status' => 200,
            ];
         }
     } 
    
}
