<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use \App\Models\Customer;

class CustomerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'admin';

    # Bind the customer model
    protected $customer;

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->middleware('guest');
        $this->customer = $customer;
    }

    /**
     *
     * Function to Customers
     *
     */
    public function customers()
    {
        $customers = $this->customer::get();
        return view($this->view.'.customers')->with(['customers' => $customers]);   
    }

    # Add Customers
    public function addCustomer(Request $request)
    {
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'customer_image' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }
        if($request->hasfile('customer_image')){
            $file = $request->file('customer_image');
            $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
            $file->move('img/', $filename);
            $customer_image='img/'.$filename;
        }

        $data = [
            'customer_image' => $customer_image,
            'status'       => '1',
        ];

        $data = $this->customer::create($data);
        if($data){
            return redirect()->back()->withErrors(['success' => 'Customer added successfully.']);
        }
    }

    # Uodate Customer
    public function updateCustomer(Request $request)
    {
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'customer_image' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }
        if($request->hasfile('customer_image')){
            $file = $request->file('customer_image');
            $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
            $file->move('img/', $filename);
            $customer_image='img/'.$filename;
        }

        $data = [
            'customer_image' => $customer_image,
            'status'       => '1',
        ];

        $data = $this->customer::find($request->get('customer_id'));
        if($data){
            $data->update(['customer_image' => $customer_image]);
            return redirect()->back()->withErrors(['success' => 'Customer updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }   

    # Change Customer Status
    public function changeCustomerStatus(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $customer = $this->customer::find($input['id']);
            if($customer->status == '1'){
                $customer = $customer->where('id', $input['id'])->update(['status' => '0']);
            }else if($customer->status == '0'){
                $customer = $customer->where('id', $input['id'])->update(['status' => '1']);
            }

            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Delete Customer
    public function deleteCustomer(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $customer = $this->customer::find($input['id']);
            $customer->delete();
            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Edit Customer
    public function editCustomer(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $customer = $this->customer::find($input['id']);
            return $response = [
                'status' => 200,
                'img' => $customer->customer_image, 
            ];
         }
     } 

    
}
