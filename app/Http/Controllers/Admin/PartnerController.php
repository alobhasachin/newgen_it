<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use \App\Models\Partner;

class PartnerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'admin';

    # Bind the partner model
    protected $partner;

    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Partner $partner)
    {
        $this->middleware('guest');
        $this->partner = $partner;
    }

    /**
     *
     * Function to Partners
     *
     */
    public function partners()
    {
        $partners = $this->partner::get();
        return view($this->view.'.partners')->with(['partners' => $partners]);   
    }

    # Add Partners
    public function addPartner(Request $request)
    {
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'partner_image' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }
        if($request->hasfile('partner_image')){
            $file = $request->file('partner_image');
            $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
            $file->move('img/', $filename);
            $partner_image='img/'.$filename;
        }

        $data = [
            'partner_image' => $partner_image,
            'status'       => '1',
        ];

        $data = $this->partner::create($data);
        if($data){
            return redirect()->back()->withErrors(['success' => 'Partner added successfully.']);
        }
    }

    # Uodate Partner
    public function updatePartner(Request $request)
    {
        # validate the email and password
        $validation = Validator::make($request->all(), [
            'partner_image' => 'required',
        ]);

        if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }
        if($request->hasfile('partner_image')){
            $file = $request->file('partner_image');
            $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
            $file->move('img/', $filename);
            $partner_image='img/'.$filename;
        }

        $data = [
            'partner_image' => $partner_image,
            'status'       => '1',
        ];

        $data = $this->partner::find($request->get('partner_id'));
        if($data){
            $data->update(['partner_image' => $partner_image]);
            return redirect()->back()->withErrors(['success' => 'Partner updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['warning' => 'Something went wrong.']);
        }
    }   

    # Change Partner Status
    public function changePartnerStatus(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $partner = $this->partner::find($input['id']);
            if($partner->status == '1'){
                $partner = $partner->where('id', $input['id'])->update(['status' => '0']);
            }else if($partner->status == '0'){
                $partner = $partner->where('id', $input['id'])->update(['status' => '1']);
            }

            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Delete Partner
    public function deletePartner(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $partner = $this->partner::find($input['id']);
            $partner->delete();
            return $response = [
                'status' => 200,
            ];
         }
     } 

     # Edit Partner
    public function editPartner(Request $request)
     {
         $input = $request->all();
         if(isset($input['id']) && $input['id'] > '0'){
            $partner = $this->partner::find($input['id']);
            return $response = [
                'status' => 200,
                'img' => $partner->partner_image, 
            ];
         }
     } 

    
}
