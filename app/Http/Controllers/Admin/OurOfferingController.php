<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use App\Models\ItService;
use App\Models\ServiceDelivery;
use App\Models\AssetManagement;
use App\Models\CloudVirtulization;
use App\Models\HrAdvisory;
use App\Models\CyberSecurity;
use App\Models\HumanResource;
use App\Models\HardwareProcurement;
use App\Models\TechManagement;

class OurOfferingController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'admin';

    #Bind the It Service Model
    protected $it_service;

    # Bind the Service Delivery Model
    protected $service_delivery;

    # Bind the Asset Management
    protected $asset_mgmt;

    # Bind the Cloud Virtulization
    protected $cloud_virtual;

    # Bind the Hr Advisory Model
    protected $hr_advisory;

    # Bind the Cyber Security Model
    protected $cyber_security;

    # Bind the Human Resource Model
    protected $human_resource;

    # Bind the Tech Management Model
    protected $tech_mgmt;

    # Bind the Hardware Procurement Model
    protected $hardware_proc;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ItService $it_service, ServiceDelivery $service_delivery, AssetManagement $asset_mgmt, CloudVirtulization $cloud_virtual, HrAdvisory $hr_advisory, CyberSecurity $cyber_security, HumanResource $human_resource, TechManagement $tech_mgmt, HardwareProcurement $hardware_proc)
    {
        $this->middleware('guest');
        $this->it_service = $it_service;
        $this->service_delivery = $service_delivery;   
        $this->asset_mgmt = $asset_mgmt;
        $this->cloud_virtual = $cloud_virtual;
        $this->hr_advisory = $hr_advisory;
        $this->cyber_security = $cyber_security;
        $this->human_resource = $human_resource;
        $this->hardware_proc = $hardware_proc;
        $this->tech_mgmt     = $tech_mgmt;
    }

    # Function to get It managed service
    public function itManagedServices()
    {
        $details = $this->it_service::first();
        return view($this->view.'.it_management')->with(['details' => $details]);
    }

    # To add the It services details
    public function AddItServices(Request $request)
    {
        $input = $request->all();

        $data = [
            'main_title' => $input['title']??'',
            'description' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['image'] = $image;
            }
        }
        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->it_service::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'It Services updated successfully']);
        }else{
            $save = $this->it_service::create($data);
            return redirect()->back()->withErrors(['success' => 'It Services added successfully']);
        }

        
    }

    # Add the services offered
    public function addServicesOffered(Request $request)
    {
        $input = $request->all();
        $data = [
            'service_offered' => $input['service_offered'],
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->it_service::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Services Offered updated successfully']);
        }else{
            $save = $this->it_service::create($data);
            return redirect()->back()->withErrors(['success' => 'Services Offered added successfully']);
        }
    }

    # Function to get Service Delivery
    public function serviceDelivery()
    {
        $details = $this->service_delivery::first();
        return view($this->view.'.service_delivery')->with(['details' => $details]);
    }
    
    # Add Serice delivery info
    public function addServiceDelivery(Request $request)
    {
        $input = $request->all();

        $data = [
            'main_title' => $input['main_title']??'',
            'description' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->service_delivery::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Service Delivery Info updated successfully']);
        }else{
            $save = $this->service_delivery::create($data);
            return redirect()->back()->withErrors(['success' => 'Service Delivery Info added successfully']);
        }
    }

    # Function to add remote support
    public function addRemoteSupport(Request $request)
    {
        $input = $request->all();

        $data = [
            'support_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->service_delivery::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => '24X7 Remote support updated successfully']);
        }else{
            $save = $this->service_delivery::create($data);
            return redirect()->back()->withErrors(['success' => '24X7 Remote support added successfully']);
        }
    }

    # Function to add technical support
    public function addTechnicalSupport(Request $request)
    {
        $input = $request->all();
        $data = [
            'tech_support_title1' => $input['title1']??'',
            'tech_support_desc1' => $input['description1']??'',
            'tech_support_title2' => $input['title2']??'',
            'tech_support_desc2' => $input['description2']??'',
            'tech_support_last_desc' => $input['last_description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['tech_support_image'] = $image;
            }
        }


        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->service_delivery::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Technical support updated successfully']);
        }else{
            $save = $this->service_delivery::create($data);
            return redirect()->back()->withErrors(['success' => 'Technical support added successfully']);
        }
    }

    # Function to add Staff Augmentation
    public function addStaffAugmentation(Request $request)
    {
        $input = $request->all();

        $data = [
            'staff_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['staff_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->service_delivery::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Staff Augmentation updated successfully']);
        }else{
            $save = $this->service_delivery::create($data);
            return redirect()->back()->withErrors(['success' => 'Staff Augmentation added successfully']);
        }
    }

    # Function to add Desktop Support
    public function addDesktopSupport(Request $request)
    {
        $input = $request->all();

        $data = [
            'desktop_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['desktop_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->service_delivery::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Desktop Support updated successfully']);
        }else{
            $save = $this->service_delivery::create($data);
            return redirect()->back()->withErrors(['success' => 'Desktop Support added successfully']);
        }
    }

    # Function to add Best Practices
    public function addBestPractices(Request $request)
    {
        $input = $request->all();

        $data = [
            'best_practices' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->service_delivery::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Best practices updated successfully']);
        }else{
            $save = $this->service_delivery::create($data);
            return redirect()->back()->withErrors(['success' => 'Best practices added successfully']);
        }
    }

    # Function to get Asset Management 
    public function assetManagement()
    {
        $details = $this->asset_mgmt::first();
        return view($this->view.'.asset_management')->with(['details' => $details]);
    }

    # Function to add or update Asset management
    public function addAssetManagement(Request $request)
    {
        $input = $request->all();

        $data = [
            'description' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->asset_mgmt::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Details updated successfully']);
        }else{
            $save = $this->asset_mgmt::create($data);
            return redirect()->back()->withErrors(['success' => 'Details added successfully']);
        }
    }

    # Function to get Cloud and Virtual
    public function cloudVirtual()
    {
        $details = $this->cloud_virtual::first();
        return view($this->view.'.cloud_virtual')->with(['details' => $details]);
    }
    
    # Function to add cloud and virtulization
    public function addCloudVirtulization(Request $request)
    {
        $input = $request->all();

        $data = [
            'desription' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Cloud and Virtulzation Details updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => 'Cloud and Virtulzation Details added successfully']);
        }
    }

    # Function to add cloud Description
    public function addCloudDescription(Request $request)
    {
        $input = $request->all();

        $data = [
            'cloud_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Cloud Description updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => 'Cloud Description added successfully']);
        }
    }

    # Function to add cloud solutions
    public function addCloudSolutions(Request $request)
    {
        $input = $request->all();

        $data = [
            'cloud_sol_desc' => $input['description1']??'',
            'hybrid_sol_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['cloud_sol_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Cloud Solutions updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => 'Cloud Solutions added successfully']);
        }
    }

    # Function to add Storage virtulizaton
    public function addStorageVirtulization(Request $request)
    {
        $input = $request->all();

        $data = [
            'storage_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['storage_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Storage Virtualization Details updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => 'Storage Virtualization Details added successfully']);
        }
    }

    # Function to add cloud solutions
    public function addDesktopVirtualization(Request $request)
    {
        $input = $request->all();

        $data = [
            'desktop_virtual_desc' => $input['description1']??'',
            'high_avail_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['desktop_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => '
Desktop Virtualization & High Availability details updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => '
Desktop Virtualization & High Availability details added successfully']);
        }
    }

    # Function to add Disaster Recovery
    public function addDisasterRecovery(Request $request)
    {
        $input = $request->all();

        $data = [
            'disaster_desc' => $input['description1']??'',
            'hosting_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['disaster_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => '
Disaster Recovery & Hosting details updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => '
Disaster Recovery & Hosting details added successfully']);
        }
    }

    # Function to add Voice Solutions
    public function addVoiceSolutions(Request $request)
    {
        $input = $request->all();

        $data = [
            'voice_desc' => $input['description1']??'',
            'software_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['voice_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => '
Voice Solutions & Software As A Service details updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => '
Voice Solutions & Software As A Service details added successfully']);
        }
    }

    # Function to add Infrastructure As a service
    public function addInfraDesc(Request $request)
    {
        $input = $request->all();

        $data = [
            'infrastructure_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Infrastructure As a Service updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => 'Infrastructure As a Service added successfully']);
        }
    }

    # Function to add server virtulizaton
    public function addServerVirtulization(Request $request)
    {
        $input = $request->all();

        $data = [
            'virtual_desc' => $input['description1']??'',
            'server_virtual_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['virtual_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cloud_virtual::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Virtualization & Server Virtualization Details updated successfully']);
        }else{
            $save = $this->cloud_virtual::create($data);
            return redirect()->back()->withErrors(['success' => 'Virtualization & Server Virtualization Details added successfully']);
        }
    }

    # Function to get HR Advisory
    public function HrAdvisory()
    {
        $details = $this->hr_advisory::first();
        return view($this->view.'.hr_advisory')->with(['details' => $details]);
    }

    # Function to add Advisory Services
    public function addAdvisoryServices(Request $request)
    {
        $input = $request->all();

        $data = [
            'service_desc' => $input['description1']??'',
            'engineering_desc' => $input['description2']??'',
            'effectiveness_desc' => $input['description3']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hr_advisory::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'HR Advisory Services details updated successfully']);
        }else{
            $save = $this->hr_advisory::create($data);
            return redirect()->back()->withErrors(['success' => 'HR Advisory Services details added successfully']);
        }
    }

    # Function to add Compesation consulting desc
    public function addCompensationConsulting(Request $request)
    {
        $input = $request->all();

        $data = [
            'compensation_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hr_advisory::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Compensation Consulting updated successfully']);
        }else{
            $save = $this->hr_advisory::create($data);
            return redirect()->back()->withErrors(['success' => 'Compensation Consulting added successfully']);
        }
    }

    # Function to add Policy and compliance
    public function addPolicyCompliance(Request $request)
    {
        $input = $request->all();

        $data = [
            'policy_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hr_advisory::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Policy & Compliance updated successfully']);
        }else{
            $save = $this->hr_advisory::create($data);
            return redirect()->back()->withErrors(['success' => 'Policy & Compliance added successfully']);
        }
    }

    # Function to add Training and development
    public function addTrainingDevelopment(Request $request)
    {
        $input = $request->all();

        $data = [
            'training_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hr_advisory::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Training & Development updated successfully']);
        }else{
            $save = $this->hr_advisory::create($data);
            return redirect()->back()->withErrors(['success' => 'Training & Development added successfully']);
        }
    }

    # Function to get Cyber Security
    public function cyberSecurity()
    {
        $details = $this->cyber_security::first();
        return view($this->view.'.cyber_security')->with(['details' => $details]);
    }

    # Function to add security details
    public function addSecurity(Request $request)
    {
        $input = $request->all();

        $data = [
            'security_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['security_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cyber_security::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Security details updated successfully']);
        }else{
            $save = $this->cyber_security::create($data);
            return redirect()->back()->withErrors(['success' => 'Security details added successfully']);
        }
    }

    # Function to add security process details
    public function addSecurityProcess(Request $request)
    {
        $input = $request->all();

        $data = [
            'process_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['process_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cyber_security::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Security process details updated successfully']);
        }else{
            $save = $this->cyber_security::create($data);
            return redirect()->back()->withErrors(['success' => 'Security process details added successfully']);
        }
    }

    # Function to add Regulatory Planning And Remediation & Access ControlS
    public function addRegularPlanning(Request $request)
    {
        $input = $request->all();

        $data = [
            'regulatory_desc' => $input['description1']??'',
            'access_control_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['regulatory_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cyber_security::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Regulatory Planning And Remediation & Access Controls details updated successfully']);
        }else{
            $save = $this->cyber_security::create($data);
            return redirect()->back()->withErrors(['success' => 'Regulatory Planning And Remediation & Access Controls details added successfully']);
        }
    }

    # Function to add Antivirus/Malware/Email Security & Bring Your Own Device
    public function addAntivirusSecurity(Request $request)
    {
        $input = $request->all();

        $data = [
            'antivirus_security_desc' => $input['description1']??'',
            'bring_your_own_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['antivirus_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cyber_security::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Antivirus/Malware/Email Security & Bring Your Own Device details updated successfully']);
        }else{
            $save = $this->cyber_security::create($data);
            return redirect()->back()->withErrors(['success' => 'Antivirus/Malware/Email Security & Bring Your Own Device details added successfully']);
        }
    }

    # Function to add Remote Access details
    public function addRemoteAccess(Request $request)
    {
        $input = $request->all();

        $data = [
            'remote_access_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->cyber_security::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Remote Access details updated successfully']);
        }else{
            $save = $this->cyber_security::create($data);
            return redirect()->back()->withErrors(['success' => 'Remote Access details added successfully']);
        }
    }

    # Function to get Tech management view
    public function techManagement()
    {
        $details = $this->tech_mgmt::first();
        return view($this->view.'.tech_management')->with(['details' => $details]);
    }

    # Function to add Infrastructure Management
    public function addInfraMgmt(Request $request)
    {
        $input = $request->all();

        $data = [
            'infra_mgmt_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['infra_mgmt_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->tech_mgmt::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Infrastructure Management details updated successfully']);
        }else{
            $save = $this->tech_mgmt::create($data);
            return redirect()->back()->withErrors(['success' => 'Infrastructure Management details added successfully']);
        }
    }

    # Function to add Network And System Management Services
    public function addNetworkService(Request $request)
    {
        $input = $request->all();

        $data = [
            'network_service_desc' => $input['description']??'',
        ];


        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->tech_mgmt::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Network And System Management Service details updated successfully']);
        }else{
            $save = $this->tech_mgmt::create($data);
            return redirect()->back()->withErrors(['success' => 'Network And System Management Service details added successfully']);
        }
    }

    # Function to add Network Management
    public function addNetworkMgmt(Request $request)
    {
        $input = $request->all();

        $data = [
            'network_mgmt_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['network_mgmt_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->tech_mgmt::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Network Management details updated successfully']);
        }else{
            $save = $this->tech_mgmt::create($data);
            return redirect()->back()->withErrors(['success' => 'Network Management details added successfully']);
        }
    }

    # Function to add Outsource Benefits
    public function addOutsourceBenefits(Request $request)
    {
        $input = $request->all();

        $data = [
            'benefits_desc' => $input['description1']??'',
            'backup_sol_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['benefits_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->tech_mgmt::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Network And System Management Outsourcing Benefits updated successfully']);
        }else{
            $save = $this->tech_mgmt::create($data);
            return redirect()->back()->withErrors(['success' => 'Network And System Management Outsourcing Benefits added successfully']);
        }
    }

     # Function to add Application Management And Database Administration
    public function addAppMgmt(Request $request)
    {
        $input = $request->all();

        $data = [
            'app_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['app_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->tech_mgmt::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Application Management And Database Administration details updated successfully']);
        }else{
            $save = $this->tech_mgmt::create($data);
            return redirect()->back()->withErrors(['success' => 'Application Management And Database Administration details added successfully']);
        }
    }

    # Function to add Monitoring And Alerting
    public function addMonitoringDesc(Request $request)
    {
        $input = $request->all();

        $data = [
            'monitoring_desc' => $input['description']??'',
        ];


        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->tech_mgmt::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Monitoring And Alerting details updated successfully']);
        }else{
            $save = $this->tech_mgmt::create($data);
            return redirect()->back()->withErrors(['success' => 'Monitoring And Alerting details added successfully']);
        }
    }

    # Function to get Human Resource Services view
    public function humanResourceServices()
    {
        $details = $this->human_resource::first();
        return view($this->view.'.human_resource')->with(['details' => $details]);
    }

    # Function to add Recruitment Services details
    public function addRecruitmentServices(Request $request)
    {
        $input = $request->all();

        $data = [
            'recruitment_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->human_resource::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Recruitment Services details updated successfully']);
        }else{
            $save = $this->human_resource::create($data);
            return redirect()->back()->withErrors(['success' => 'Recruitment Services details added successfully']);
        }
    }

    # Function to add Sourcing, Recruiting, Hiring and On-Boarding
    public function addOtherServices(Request $request)
    {
        $input = $request->all();

        $data = [
            'sourcing_desc' => $input['description1']??'',
            'recruting_desc' => $input['description2']??'',
            'hiring_desc' => $input['description3']??'',
            'on_boarding_desc' => $input['description4']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['hiring_section_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->human_resource::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Sourcing, Recruiting, Hiring and On-Boarding details updated successfully']);
        }else{
            $save = $this->human_resource::create($data);
            return redirect()->back()->withErrors(['success' => 'Sourcing, Recruiting, Hiring and On-Boarding details added successfully']);
        }
    }

    # Function to add Human Resource Details
    public function addHumanResource(Request $request)
    {
        $input = $request->all();

        $data = [
            'out_source_desc' => $input['description']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['resource_image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->human_resource::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'HUMAN RESOURCE MANAGEMENT SIMPLIFIED details updated successfully']);
        }else{
            $save = $this->human_resource::create($data);
            return redirect()->back()->withErrors(['success' => 'HUMAN RESOURCE MANAGEMENT SIMPLIFIED details added successfully']);
        }
    }

    # Function to get Haerdware Software Procurement view
    public function hardwareSoftwareProcurement()
    {
        $details = $this->hardware_proc::first();
        return view($this->view.'.hardware_software')->with(['details' => $details]);
    }

    # Function to add Hardware & Software Procurement
    public function addHardwareProc(Request $request)
    {
        $input = $request->all();

        $data = [
            'hw_sw_procurement_desc' => $input['description1']??'',
            'full_range_desc' => $input['description2']??'',
        ];

        if(isset($input['image']) && !empty($input['image'])){
            if($request->hasfile('image')){
                $file = $request->file('image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $image='img/'.$filename;
                $data['image'] = $image;
            }
        }

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hardware_proc::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Hardware & Software Procurement details updated successfully']);
        }else{
            $save = $this->hardware_proc::create($data);
            return redirect()->back()->withErrors(['success' => 'Hardware & Software Procurement details added successfully']);
        }
    }

    # Function to add Infrastructure Equipment
    public function addInfraEquipment(Request $request)
    {
        $input = $request->all();

        $data = [
            'equipment_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hardware_proc::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Infrastructure Equipment details updated successfully']);
        }else{
            $save = $this->hardware_proc::create($data);
            return redirect()->back()->withErrors(['success' => 'Infrastructure Equipment details added successfully']);
        }
    }

    # Function to add Infrastructure Software
    public function addInfraSoftware(Request $request)
    {
        $input = $request->all();

        $data = [
            'software_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hardware_proc::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Infrastructure Software details updated successfully']);
        }else{
            $save = $this->hardware_proc::create($data);
            return redirect()->back()->withErrors(['success' => 'Infrastructure Software details added successfully']);
        }
    }

    # Function to add End user Equipment
    public function addEndUserEquipment(Request $request)
    {
        $input = $request->all();

        $data = [
            'end_user_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hardware_proc::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'End User Equipment details updated successfully']);
        }else{
            $save = $this->hardware_proc::create($data);
            return redirect()->back()->withErrors(['success' => 'End User Equipment details added successfully']);
        }
    }

    # Function to add Other Equipment
    public function addOtherEquipment(Request $request)
    {
        $input = $request->all();

        $data = [
            'other_desc' => $input['description']??'',
        ];

        if(isset($input['id']) && $input['id'] > 0){
            $update = $this->hardware_proc::find($input['id']);
            $update->update($data);
            return redirect()->back()->withErrors(['success' => 'Other Equipment details updated successfully']);
        }else{
            $save = $this->hardware_proc::create($data);
            return redirect()->back()->withErrors(['success' => 'Other Equipment details added successfully']);
        }
    }


}
