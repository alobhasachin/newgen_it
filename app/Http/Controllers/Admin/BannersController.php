<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use App\Models\Banner;

class BannersController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'admin';

    # Bind the Banner Model
    protected $banner;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Banner $banner)
    {
        $this->middleware('guest');
        $this->banner = $banner;
    }

    # Get Banners view
    public function Banners()
    {
        $banners = $this->banner::first();
        return view($this->view.'.inner_banner')->with(['banners' => $banners]);
    }

    # Save and update About Banner

    public function aboutBanner(Request $request)
    {
        $input = $request->all();

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
            'about_banner' => 'required',
            ]);

            if($validation->fails()) {  
                return redirect()->back()->withErrors($validation);
            }
        }
        $update_data = [];
        if(!empty($input['about_banner']) && $input['about_banner'] != null){
            if($request->hasfile('about_banner')){
                $file = $request->file('about_banner');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $about_banner='img/'.$filename;
            }
            $update_data = [
            'about_banner' => $about_banner,
            ];
        }

        
        if(!empty($update_data)){
            if(isset($input['id']) && $input['id'] > '0'){
                $data = $this->banner::find($input['id']);
                $data->update($update_data);
            }else{
                $data = $this->banner::create($update_data);
            }
        }else{
            return redirect()->back();
        }
        

        if($data){
            return redirect()->back()->withErrors(['success' => 'Banner updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['success' => 'Something went wrong']);
        }

    }

    # Save and update Partner Banner

    public function partnerBanner(Request $request)
    {
        
        $input = $request->all();

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
                'partner_banner' => 'required',
            ]);

            if($validation->fails()) {  
                return redirect()->back()->withErrors($validation);
            }
        }    
        $update_data = [];
        if(!empty($input['partner_banner']) && $input['partner_banner'] != null){
            if($request->hasfile('partner_banner')){
                $file = $request->file('partner_banner');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $partner_banner='img/'.$filename;
            }
            $update_data = [
                'partner_banner' => $partner_banner,
            ];
        }    

        

        if(!empty($update_data)){
            if(isset($input['id']) && $input['id'] > '0'){
                $data = $this->banner::find($input['id']);
                $data->update($update_data);
            }else{
                $data = $this->banner::create($update_data);
            }
        }else{
            return redirect()->back();
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Banner updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['success' => 'Something went wrong']);
        }
    }

    # Save and update Customer Banner

    public function customerBanner(Request $request)
    {
        
        $input = $request->all();

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
                'customer_banner' => 'required',
            ]);

            if($validation->fails()) {  
                return redirect()->back()->withErrors($validation);
            }
        }

        $update_data = [];
        if(!empty($input['customer_banner']) && $input['customer_banner'] != null){   
            if($request->hasfile('customer_banner')){
                $file = $request->file('customer_banner');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $customer_banner='img/'.$filename;
            }

            $update_data = [
                'customer_banner' => $customer_banner,
            ];
        }    

        

        if(!empty($update_data)){
            if(isset($input['id']) && $input['id'] > '0'){
                $data = $this->banner::find($input['id']);
                $data->update($update_data);
            }else{
                $data = $this->banner::create($update_data);
            }
        }else{
            return redirect()->back();
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Banner updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['success' => 'Something went wrong']);
        }
    }


    # Save and update Career Banner

    public function careerBanner(Request $request)
    {
        
        $input = $request->all();

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
                'career_banner' => 'required',
            ]);

            if($validation->fails()) {  
                return redirect()->back()->withErrors($validation);
            }
        }    

        $update_data = [];
        if(!empty($input['career_banner']) && $input['career_banner'] != null){ 
            if($request->hasfile('career_banner')){
                $file = $request->file('career_banner');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $career_banner='img/'.$filename;
            }

            $update_data = [
                'career_banner' => $career_banner,
            ];
        }    

        

        if(!empty($update_data)){
            if(isset($input['id']) && $input['id'] > '0'){
                $data = $this->banner::find($input['id']);
                $data->update($update_data);
            }else{
                $data = $this->banner::create($update_data);
            }
        }else{
            return redirect()->back();
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Banner updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['success' => 'Something went wrong']);
        }
    }

    # Save and update Contact Banner

    public function contactBanner(Request $request)
    {
        
        $input = $request->all();

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
                'contact_banner' => 'required',
            ]);

            if($validation->fails()) {  
                return redirect()->back()->withErrors($validation);
            }
        }    

        $update_data = [];
        if(!empty($input['contact_banner']) && $input['contact_banner'] != null){
            if($request->hasfile('contact_banner')){
                $file = $request->file('contact_banner');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $contact_banner='img/'.$filename;
            }
            $update_data = [
                'contact_banner' => $contact_banner,
            ];
        }    

        

        if(!empty($update_data)){
            if(isset($input['id']) && $input['id'] > '0'){
                $data = $this->banner::find($input['id']);
                $data->update($update_data);
            }else{
                $data = $this->banner::create($update_data);
            }
        }else{
            return redirect()->back();
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Banner updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['success' => 'Something went wrong']);
        }
    }

    

    
}
