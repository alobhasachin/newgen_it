<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Validator;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $view = 'admin';

    # Bind the Contact Model
    protected $contact;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->middleware('guest');
        $this->contact = $contact;
    }

    # Get Contact view
    public function Contact()
    {
        $details = $this->contact::first();
        return view($this->view.'.contact')->with(['details' => $details]);
    }

    # save or update contact details
    public function saveContactDetails(Request $request)
    {
        $input = $request->all();

        if(empty($input['id']) && $input['id'] == null){
            $validation = Validator::make($request->all(), [
                'contact_image' => 'required',
            ]);
            if($validation->fails()) {  
            return redirect()->back()->withErrors($validation);
        }
        }

        
        $update_data = [
            'contact_info' => json_encode($input['contact_info']),
        ];

        if(isset($input['contact_image']) && !empty($input['contact_image'])){
            if($request->hasfile('contact_image')){
                $file = $request->file('contact_image');
                $filename =((string)(microtime(true)*10000))."-".$file->getClientOriginalName();
                $file->move('img/', $filename);
                $contact_image='img/'.$filename;
            }
            $update_data['contact_image'] = $contact_image;
        }
        
        if(isset($input['id']) && $input['id'] > '0'){
            $data = $this->contact::find($input['id']);
            $data->update($update_data);
        }else{
            $data = $this->contact::create($update_data);
        }

        if($data){
            return redirect()->back()->withErrors(['success' => 'Contact Details updated successfully.']);
        }else{
            return redirect()->back()->withErrors(['success' => 'Something went wrong']);
        }
        
    }
    
}
