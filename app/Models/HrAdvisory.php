<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HrAdvisory extends Model
{
    

    # define table
    protected $table ='hr_advisories';
    
    # define fillable fildes
    protected $fillable = [
  	                      'service_desc',
  	                      'engineering_desc',
  	                      'effectiveness_desc',
  	                      'image',
  	                      'compensation_desc',
                          'policy_desc',
                          'training_desc',
                          ];
}
