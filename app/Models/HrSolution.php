<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HrSolution extends Model
{
    

    # define table
    protected $table ='hr_solutions';
    
    # define fillable fildes
    protected $fillable = [
  	                      'main_title',
  	                      'main_description',
  	                      'sub_title',
  	                      'sub_description'
                          ];
}
