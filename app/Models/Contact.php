<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    

    # define table
    protected $table ='contacts';
    
    # define fillable fildes
    protected $fillable = [
  	                      'contact_image',
  	                      'contact_info'
                          ];
}
