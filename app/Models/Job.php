<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    

    # define table
    protected $table ='jobs';
    
    # define fillable fildes
    protected $fillable = [
  	                      'job_title',
  	                      'description',
  	                      'status',
                          ];
}
