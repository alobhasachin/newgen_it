<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CareerOverview extends Model
{
    

    # define table
    protected $table ='career_overview';
    
    # define fillable fildes
    protected $fillable = [
  	                      'title1',
  	                      'title1_desc',
  	                      'title2',
  	                      'title2_desc',
                          ];
}
