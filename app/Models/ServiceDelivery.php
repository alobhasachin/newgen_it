<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceDelivery extends Model
{
    

    # define table
    protected $table ='service_deliveries';
    
    # define fillable fildes
    protected $fillable = [
  	                      'main_title',
  	                      'description',
  	                      'image',
  	                      'support_desc',
  	                      'tech_support_title1',
                          'tech_support_desc1',
                          'tech_support_title2',
                          'tech_support_desc2',
                          'tech_support_image',
                          'tech_support_last_desc',
                          'staff_image',
                          'staff_desc',
                          'desktop_image',
                          'desktop_desc',
                          'best_practices'
                          ];
}
