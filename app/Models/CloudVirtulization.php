<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CloudVirtulization extends Model
{
    

    # define table
    protected $table ='cloud_virtualizations';
    
    # define fillable fildes
    protected $fillable = [
  	                      'desription',
  	                      'image',
  	                      'cloud_desc',
  	                      'cloud_sol_desc',
  	                      'cloud_sol_image',
                          'hybrid_sol_desc',
                          'virtual_desc',
                          'server_virtual_desc',
                          'virtual_image',
                          'storage_desc',
                          'storage_image',
                          'desktop_image',
                          'desktop_virtual_desc',
                          'high_avail_desc',
                          'disaster_desc',
                          'disaster_image',
                          'hosting_desc',
                          'voice_desc',
                          'software_desc',
                          'voice_image',
                          'infrastructure_desc'
                          ];
}
