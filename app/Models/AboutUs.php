<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    

    # define table
    protected $table ='about_us';
    
    # define fillable fildes
    protected $fillable = [
  	                      'title',
  	                      'description',
  	                      'image'
                          ];
}
