<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    

    # define table
    protected $table ='services';
    
    # define fillable fildes
    protected $fillable = [
  	                      'name',
  	                      'description',
  	                      'image',
  	                      'status'
                          ];
}
