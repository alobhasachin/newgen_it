<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    

    # define table
    protected $table ='partners';
    
    # define fillable fildes
    protected $fillable = [
  	                      'partner_image',
  	                      'status'
                          ];
}
