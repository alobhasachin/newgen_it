<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderBanner extends Model
{
    

    # define table
    protected $table ='slider_banners';
    
    # define fillable fildes
    protected $fillable = [
  	                      'banner_image',
                          ];
}
