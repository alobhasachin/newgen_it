<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HardwareProcurement extends Model
{
    

    # define table
    protected $table ='hw_sw_procurements';
    
    # define fillable fildes
    protected $fillable = [
  	                      'hw_sw_procurement_desc',
  	                      'full_range_desc',
  	                      'image',
  	                      'equipment_desc',
  	                      'software_desc',
                          'end_user_desc',
                          'other_desc',
                          ];
}
