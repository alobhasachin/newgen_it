<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssetManagement extends Model
{
    

    # define table
    protected $table ='asset_managements';
    
    # define fillable fildes
    protected $fillable = [
  	                      'description',
  	                      'image'
                          ];
}
