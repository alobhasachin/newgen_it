<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    

    # define table
    protected $table ='settings';
    
    # define fillable fildes
    protected $fillable = [
  	                      'website_footer_summary',
  	                      'copyright_summary',
  	                      'website_logo',
  	                      'admin_logo',
  	                      'favicon_icon'
                          ];
}
