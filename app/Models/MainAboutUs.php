<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainAboutUs extends Model
{
    

    # define table
    protected $table ='main_about_us';
    
    # define fillable fildes
    protected $fillable = [
  	                      'title',
  	                      'main_description',
  	                      'main_image',
  	                      'core_value',
  	                      'teamwork',
  	                      'spirit',
  	                      'responsibility',
  	                      'focus',
  	                      'mission',
  	                      'vision',
  	                      'description2',
  	                      'image',
                          ];
}
