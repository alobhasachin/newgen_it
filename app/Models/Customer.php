<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    

    # define table
    protected $table ='customers';
    
    # define fillable fildes
    protected $fillable = [
  	                      'customer_image',
  	                      'status'
                          ];
}
