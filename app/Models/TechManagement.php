<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TechManagement extends Model
{
    

    # define table
    protected $table ='tech_managements';
    
    # define fillable fildes
    protected $fillable = [
  	                      'infra_mgmt_desc',
  	                      'infra_mgmt_image',
  	                      'network_service_desc',
  	                      'network_mgmt_image',
  	                      'network_mgmt_desc',
                          'benefits_desc',
                          'benefits_image',
                          'backup_sol_desc',
                          'app_image',
                          'app_desc',
                          'monitoring_desc',
                          ];
}
