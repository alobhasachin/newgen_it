<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HumanResource extends Model
{
    

    # define table
    protected $table ='human_resources';
    
    # define fillable fildes
    protected $fillable = [
  	                      'recruitment_desc',
  	                      'sourcing_desc',
  	                      'recruting_desc',
  	                      'hiring_desc',
  	                      'on_boarding_desc',
                          'hiring_section_image',
                          'resource_image',
                          'out_source_desc',
                          ];
}
