<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    

    # define table
    protected $table ='banners';
    
    # define fillable fildes
    protected $fillable = [
  	                      'about_banner',
  	                      'partner_banner',
  	                      'customer_banner',
  	                      'career_banner',
  	                      'contact_banner'
                          ];
}
