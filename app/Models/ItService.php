<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItService extends Model
{
    

    # define table
    protected $table ='it_services';
    
    # define fillable fildes
    protected $fillable = [
  	                      'main_title',
  	                      'description',
  	                      'image',
  	                      'service_offered'
                          ];
}
