<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CyberSecurity extends Model
{
    

    # define table
    protected $table ='cyber_securities';
    
    # define fillable fildes
    protected $fillable = [
  	                      'security_desc',
  	                      'security_image',
  	                      'process_desc',
  	                      'process_image',
  	                      'regulatory_desc',
  	                      'regulatory_image',
  	                      'access_control_desc',
  	                      'antivirus_security_desc',
  	                      'bring_your_own_desc',
  	                      'antivirus_image',
  	                      'remote_access_desc',
                          ];
}
