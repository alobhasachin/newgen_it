<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\HomeController@home')->name('/');
Route::get('/about-us', 'Frontend\HomeController@aboutUs')->name('about-us');
Route::get('/our-partners', 'Frontend\HomeController@Partners')->name('our-partners');
Route::get('/customer-base', 'Frontend\HomeController@Customers')->name('customer-base');
Route::get('/careers', 'Frontend\HomeController@Career')->name('careers');
Route::get('/contact-us', 'Frontend\HomeController@Contact')->name('contact-us');



Route::group(['prefix'=>'admin'], function(){
	# to the admin dashboard
	Route::get('/dashboard', 'Admin\AdminController@dashboard')->name('dashboard');
	Route::get('/', 'Admin\AdminController@dashboard')->name('dashboard');

	# to the slider banner
	Route::get('/slider-banner', 'Admin\AdminController@sliderBanners')->name('slider-banner');
	Route::post('/add-slider-banner', 'Admin\AdminController@addSliderBanner')->name('add-slider-banner');
	Route::post('/change-banner-status', 'Admin\AdminController@changeBannerStatus')->name('change-banner-status');
	Route::post('/delete-banner', 'Admin\AdminController@deleteBanner')->name('delete-banner');
	Route::post('/edit-banner', 'Admin\AdminController@editBanner')->name('edit-banner');
	Route::post('/update-slider-banner', 'Admin\AdminController@updateSliderBanner')->name('update-slider-banner');

	# to the about
	Route::get('/about', 'Admin\AdminController@about')->name('about');
	Route::post('/save-about-details', 'Admin\AdminController@saveAboutDetails')->name('save-about-details');


	# to the services
	Route::get('/services', 'Admin\AdminController@services')->name('services');
	Route::post('/add-service', 'Admin\AdminController@addService')->name('add-service');
	Route::post('/change-service-status', 'Admin\AdminController@changeServiceStatus')->name('change-service-status');
	Route::post('/delete-service', 'Admin\AdminController@deleteService')->name('delete-service');
	Route::post('/edit-service', 'Admin\AdminController@editService')->name('edit-service');
	Route::post('/update-service', 'Admin\AdminController@updateService')->name('update-service');

	# to the Hr solutions
	Route::get('/hr-solutions', 'Admin\AdminController@hrSolutions')->name('hr-solutions');
	Route::post('/save-hr-solutions', 'Admin\AdminController@saveHrSolutions')->name('save-hr-solutions');

	# to the Customers
	Route::get('/customers', 'Admin\CustomerController@customers')->name('customers');
	Route::post('/add-customer', 'Admin\CustomerController@addCustomer')->name('add-customer');
	Route::post('/change-customer-status', 'Admin\CustomerController@changeCustomerStatus')->name('change-customer-status');
	Route::post('/delete-customer', 'Admin\CustomerController@deleteCustomer')->name('delete-customer');
	Route::post('/edit-customer', 'Admin\CustomerController@editCustomer')->name('edit-customer');
	Route::post('/update-customer', 'Admin\CustomerController@updateCustomer')->name('update-customer');
	
	# to the Settings
	Route::get('/settings', 'Admin\AdminController@settings')->name('settings');
	Route::post('/save-settings', 'Admin\AdminController@saveSettings')->name('save-settings');

	Route::get('/who-we-are', 'Admin\AboutUsController@whoWeAre')->name('who-we-are');
	Route::post('/save-who-we-are', 'Admin\AboutUsController@saveWhoWeAre')->name('save-who-we-are');


	Route::get('/core-values', 'Admin\AboutUsController@coreValues')->name('core-values');
	Route::post('/save-core-values', 'Admin\AboutUsController@saveCoreValues')->name('save-core-values');


	Route::get('/mission_vision', 'Admin\AboutUsController@missionVision')->name('mission_vision');
	Route::post('/save-mission-vision', 'Admin\AboutUsController@saveMissionVision')->name('save-mission-vision');


	Route::get('/leadership', 'Admin\AboutUsController@leadership')->name('leadership');
	Route::post('/save-leadership', 'Admin\AboutUsController@saveLeadership')->name('save-leadership');


	Route::get('/partners', 'Admin\PartnerController@partners')->name('partners');
	Route::post('/add-partner', 'Admin\PartnerController@addPartner')->name('add-partner');
	Route::post('/change-partner-status', 'Admin\PartnerController@changePartnerStatus')->name('change-partner-status');
	Route::post('/delete-partner', 'Admin\PartnerController@deletePartner')->name('delete-partner');
	Route::post('/edit-partner', 'Admin\PartnerController@editPartner')->name('edit-partner');
	Route::post('/update-partner', 'Admin\PartnerController@updatePartner')->name('update-partner');


	Route::get('/career', 'Admin\CareerController@Career')->name('career');
	Route::post('/save-career-overview', 'Admin\CareerController@saveCareerOverview')->name('save-career-overview');

	Route::post('/add-job', 'Admin\CareerController@addJob')->name('add-job');
	Route::post('/change-job-status', 'Admin\CareerController@changeJobStatus')->name('change-job-status');
	Route::post('/delete-job', 'Admin\CareerController@deleteJob')->name('delete-job');
	Route::post('/edit-job', 'Admin\CareerController@editJob')->name('edit-job');
	Route::post('/update-job', 'Admin\CareerController@updateJob')->name('update-job');

	Route::get('/contact', 'Admin\ContactController@Contact')->name('contact');
	Route::post('/save-contact-details', 'Admin\ContactController@saveContactDetails')->name('save-contact-details');

	Route::get('/banners', 'Admin\BannersController@Banners')->name('banners');
	Route::post('/save-about-banner', 'Admin\BannersController@aboutBanner')->name('save-about-banner');
	Route::post('/save-partner-banner', 'Admin\BannersController@partnerBanner')->name('save-partner-banner');
	Route::post('/save-customer-banner', 'Admin\BannersController@customerBanner')->name('save-customer-banner');
	Route::post('/save-career-banner', 'Admin\BannersController@careerBanner')->name('save-career-banner');
	Route::post('/save-contact-banner', 'Admin\BannersController@contactBanner')->name('save-contact-banner');

	Route::get('/it-managed-services', 'Admin\OurOfferingController@itManagedServices')->name('it-managed-services');
	Route::post('/add-it-services', 'Admin\OurOfferingController@addItServices')->name('add-it-services');
	Route::post('/add-services-offered', 'Admin\OurOfferingController@addServicesOffered')->name('add-services-offered');


	Route::get('/service-delivery', 'Admin\OurOfferingController@serviceDelivery')->name('service-delivery');
	Route::post('/add-service-delivery', 'Admin\OurOfferingController@addServiceDelivery')->name('add-service-delivery');
	Route::post('/add-remote-support', 'Admin\OurOfferingController@addRemoteSupport')->name('add-remote-support');	
	Route::post('/add-technical-support', 'Admin\OurOfferingController@addTechnicalSupport')->name('add-technical-support');
	Route::post('/add-staff-augmentation', 'Admin\OurOfferingController@addStaffAugmentation')->name('add-staff-augmentation');
	Route::post('/add-desktop-support', 'Admin\OurOfferingController@addDesktopSupport')->name('add-desktop-support');
	Route::post('/add-best-practices', 'Admin\OurOfferingController@addBestPractices')->name('add-best-practices');

	Route::get('/asset-management', 'Admin\OurOfferingController@assetManagement')->name('asset-management');
	Route::post('/add-asset-management', 'Admin\OurOfferingController@addAssetManagement')->name('add-asset-management');

	Route::get('/cloud-virtual', 'Admin\OurOfferingController@cloudVirtual')->name('cloud-virtual');
	Route::post('/add-cloud-virtulization', 'Admin\OurOfferingController@addCloudVirtulization')->name('add-cloud-virtulization');
	Route::post('/add-cloud-description', 'Admin\OurOfferingController@addCloudDescription')->name('add-cloud-description');
	Route::post('/add-cloud-solutions', 'Admin\OurOfferingController@addCloudSolutions')->name('add-cloud-solutions');
	Route::post('/add-infra-desc', 'Admin\OurOfferingController@addInfraDesc')->name('add-infra-desc');
	Route::post('/add-server-virtulization', 'Admin\OurOfferingController@addServerVirtulization')->name('add-server-virtulization');
	Route::post('/add-storage-virtulization', 'Admin\OurOfferingController@addStorageVirtulization')->name('add-storage-virtulization');
	Route::post('/add-desktop-virtulization', 'Admin\OurOfferingController@addDesktopVirtualization')->name('add-desktop-virtulization');
	Route::post('/add-disaster-recovery', 'Admin\OurOfferingController@addDisasterRecovery')->name('add-disaster-recovery');
	Route::post('/add-voice-solutions', 'Admin\OurOfferingController@addVoiceSolutions')->name('add-voice-solutions');

	Route::get('/cyber-security', 'Admin\OurOfferingController@cyberSecurity')->name('cyber-security');
	Route::post('/add-security', 'Admin\OurOfferingController@addSecurity')->name('add-security');
	Route::post('/add-security-process', 'Admin\OurOfferingController@addSecurityProcess')->name('add-security-process');
	Route::post('/add-regular-planning', 'Admin\OurOfferingController@addRegularPlanning')->name('add-regular-planning');
	Route::post('/add-antivirus-security', 'Admin\OurOfferingController@addAntivirusSecurity')->name('add-antivirus-security');
	Route::post('/add-remote-access', 'Admin\OurOfferingController@addRemoteAccess')->name('add-remote-access');	




	Route::get('/hr-advisory', 'Admin\OurOfferingController@HrAdvisory')->name('hr-advisory');
	Route::post('/add-advisory-services', 'Admin\OurOfferingController@addAdvisoryServices')->name('add-advisory-services');
	Route::post('/add-compensation-consulting', 'Admin\OurOfferingController@addCompensationConsulting')->name('add-compensation-consulting');
	Route::post('/add-policy-compliance', 'Admin\OurOfferingController@addPolicyCompliance')->name('add-policy-compliance');
	Route::post('/add-training-development', 'Admin\OurOfferingController@addTrainingDevelopment')->name('add-training-development');

	Route::get('/technology-management', 'Admin\OurOfferingController@techManagement')->name('technology-management');
	Route::post('/add-infra-mgmt', 'Admin\OurOfferingController@addInfraMgmt')->name('add-infra-mgmt');
	Route::post('/add-network-service', 'Admin\OurOfferingController@addNetworkService')->name('add-network-service');
	Route::post('/add-network-mgmt', 'Admin\OurOfferingController@addNetworkMgmt')->name('add-network-mgmt');
	Route::post('/add-outsource-benefits', 'Admin\OurOfferingController@addOutsourceBenefits')->name('add-outsource-benefits');
	Route::post('/add-app-mgmt', 'Admin\OurOfferingController@addAppMgmt')->name('add-app-mgmt');
	Route::post('/add-monitoring-desc', 'Admin\OurOfferingController@addMonitoringDesc')->name('add-monitoring-desc');


	Route::get('/hardware-software-procurement', 'Admin\OurOfferingController@hardwareSoftwareProcurement')->name('hardware-software-procurement');
	Route::post('/add-hardware-proc', 'Admin\OurOfferingController@addHardwareProc')->name('add-hardware-proc');
	Route::post('/add-infra-equipment', 'Admin\OurOfferingController@addInfraEquipment')->name('add-infra-equipment');
	Route::post('/add-infra-software', 'Admin\OurOfferingController@addInfraSoftware')->name('add-infra-software');
	Route::post('/add-infra-software', 'Admin\OurOfferingController@addInfraSoftware')->name('add-infra-software');
	Route::post('/add-end-user-equipment', 'Admin\OurOfferingController@addEndUserEquipment')->name('add-end-user-equipment');
	Route::post('/add-other-equipment', 'Admin\OurOfferingController@addOtherEquipment')->name('add-other-equipment');

	Route::get('/human-resource-services', 'Admin\OurOfferingController@humanResourceServices')->name('human-resource-services');
	Route::post('/add-recruitment-services', 'Admin\OurOfferingController@addRecruitmentServices')->name('add-recruitment-services');
	Route::post('/add-other-services', 'Admin\OurOfferingController@addOtherServices')->name('add-other-services');
	Route::post('/add-human-resource', 'Admin\OurOfferingController@addHumanResource')->name('add-human-resource');


});