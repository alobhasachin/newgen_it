<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudVirtualizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_virtualizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('desription')->nullable();
            $table->longText('image')->nullable();
            $table->longText('cloud_desc')->nullable();
            $table->longText('cloud_sol_desc')->nullable();
            $table->longText('cloud_sol_image')->nullable();
            $table->longText('hybrid_sol_desc')->nullable();
            $table->longText('virtual_desc')->nullable();
            $table->longText('server_virtual_desc')->nullable();
            $table->longText('virtual_image')->nullable();
            $table->longText('storage_desc')->nullable();
            $table->longText('storage_image')->nullable();
            $table->longText('desktop_image')->nullable();
            $table->longText('desktop_virtual_desc')->nullable();
            $table->longText('high_avail_desc')->nullable();
            $table->longText('disaster_desc')->nullable();
            $table->longText('disaster_image')->nullable();
            $table->longText('hosting_desc')->nullable();
            $table->longText('voice_desc')->nullable();
            $table->longText('software_desc')->nullable();
            $table->longText('voice_image')->nullable();
            $table->longText('infrastructure_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloud_virtualizations');
    }
}
