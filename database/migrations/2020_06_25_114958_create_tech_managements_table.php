<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechManagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tech_managements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('infra_mgmt_desc')->nullable();
            $table->longText('infra_mgmt_image')->nullable();
            $table->longText('network_service_desc')->nullable();
            $table->longText('network_mgmt_image')->nullable();
            $table->longText('network_mgmt_desc')->nullable();
            $table->longText('benefits_desc')->nullable();
            $table->longText('benefits_image')->nullable();
            $table->longText('backup_sol_desc')->nullable();
            $table->longText('app_image')->nullable();
            $table->longText('app_desc')->nullable();
            $table->longText('monitoring_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tech_managements');
    }
}
