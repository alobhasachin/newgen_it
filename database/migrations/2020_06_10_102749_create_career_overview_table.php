<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerOverviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_overview', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('title1')->nullable();
            $table->longText('title1_desc')->nullable();
            $table->longText('title2')->nullable();
            $table->longText('title2_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('career_overview');
    }
}
