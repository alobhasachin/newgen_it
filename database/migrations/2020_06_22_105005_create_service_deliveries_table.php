<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_deliveries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('main_title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('image')->nullable();
            $table->longText('support_desc')->nullable();
            $table->longText('tech_support_title1')->nullable();
            $table->longText('tech_support_desc1')->nullable();
            $table->longText('tech_support_title2')->nullable();
            $table->longText('tech_support_desc2')->nullable();
            $table->longText('tech_support_image')->nullable();
            $table->longText('tech_support_last_desc')->nullable();
            $table->longText('staff_image')->nullable();
            $table->longText('staff_desc')->nullable();
            $table->longText('desktop_image')->nullable();
            $table->longText('desktop_desc')->nullable();
            $table->longText('best_practices')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_deliveries');
    }
}
