<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('it_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('main_title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('image')->nullable();
            $table->longText('service_offered')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('it_services');
    }
}
