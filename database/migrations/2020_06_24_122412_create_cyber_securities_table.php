<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyberSecuritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cyber_securities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('security_desc')->nullable();
            $table->longText('security_image')->nullable();
            $table->longText('process_desc')->nullable();
            $table->longText('process_image')->nullable();
            $table->longText('regulatory_desc')->nullable();
            $table->longText('regulatory_image')->nullable();
            $table->longText('access_control_desc')->nullable();
            $table->longText('antivirus_security_desc')->nullable();
            $table->longText('bring_your_own_desc')->nullable();
            $table->longText('antivirus_image')->nullable();
            $table->longText('remote_access_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cyber_securities');
    }
}
