<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrAdvisoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_advisories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('service_desc')->nullable();
            $table->longText('engineering_desc')->nullable();
            $table->longText('effectiveness_desc')->nullable();
            $table->longText('image')->nullable();
            $table->longText('compensation_desc')->nullable();
            $table->longText('policy_desc')->nullable();
            $table->longText('training_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_advisories');
    }
}
