<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHumanResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('human_resources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('recruitment_desc')->nullable();
            $table->longText('sourcing_desc')->nullable();
            $table->longText('recruting_desc')->nullable();
            $table->longText('hiring_desc')->nullable();
            $table->longText('on_boarding_desc')->nullable();
            $table->longText('hiring_section_image')->nullable();
            $table->longText('resource_image')->nullable();
            $table->longText('out_source_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('human_resources');
    }
}
