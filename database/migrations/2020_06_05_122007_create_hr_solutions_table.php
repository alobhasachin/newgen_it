<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrSolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_solutions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('main_title')->nullable();
            $table->longText('main_description')->nullable();
            $table->string('sub_title')->nullable();
            $table->longText('sub_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_solutions');
    }
}
