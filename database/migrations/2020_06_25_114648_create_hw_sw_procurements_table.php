<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHwSwProcurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hw_sw_procurements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('hw_sw_procurement_desc')->nullable();
            $table->longText('full_range_desc')->nullable();
            $table->longText('image')->nullable();
            $table->longText('equipment_desc')->nullable();
            $table->longText('software_desc')->nullable();
            $table->longText('end_user_desc')->nullable();
            $table->longText('other_desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hw_sw_procurements');
    }
}
